<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 33)->unique();

            $table->string('purchasable_type', 50)->nullable();
            $table->integer('purchasable_id')->unsigned();

            $table->unsignedInteger('product_id');
            $table->string('name')->nullable();
            $table->unsignedDecimal('price', 5, 2);

            // $table->tinyInteger('subscription')->default(0);
            // $table->dateTime('subscription_duration')->nullable();
            // $table->timestamp('subscription_ends_at')->nullable();

            $table->string('activate_form_url')->nullable();

            $table->string('stripe_id')->nullable();
            $table->string('charge_id')->nullable();

            $table->string('status')->default('Inactive'); // Inactive, Awaiting Activation, Working On, Completed
            $table->unsignedInteger('completion')->default(0);

            // $table->boolean('cancelled')->default(false);
            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('disabled')->default(0);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
