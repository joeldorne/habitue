<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('product_service_id')->nullable();

            // $table->string('purchasable_type', 50)->nullable();
            // $table->integer('purchasable_id')->unsigned();

            // $table->unsignedInteger('product_id');
            $table->string('nickname')->nullable();
            $table->string('name')->nullable();
            // $table->unsignedDecimal('price', 5, 2);
            
            $table->string('stripe_id')->nullable();
            $table->string('stripe_plan')->nullable();
            $table->integer('quantity');

            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('ends_at')->nullable();

            // $table->boolean('cancelled')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
