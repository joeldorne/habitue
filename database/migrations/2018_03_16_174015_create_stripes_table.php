<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('stripecustomerable_type', 50)->nullable();
            $table->integer('stripecustomerable_id')->unsigned();

            $table->string('stripe_id');
            $table->string('default_card_brand')->nullable();
            $table->string('default_card_last_four')->nullable();

            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('disabled')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripes');
    }
}
