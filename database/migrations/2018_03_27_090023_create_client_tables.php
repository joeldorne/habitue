<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by')->nullable();
            // $table->unsignedInteger('agency_id')->nullable();
            $table->string('slug', 23)->unique();
            $table->string('name', 50);
            $table->string('website_url')->nullable();
            $table->string('country', 5)->nullable();
            $table->string('phone')->nullable();
            $table->string('subdomain', 15)->nullable();
            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('disabled')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('client_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('client_id');
            $table->string('position')->nullable();
            $table->timestamps();

            $table->unique(['user_id', 'client_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });

        // Schema::create('client_user', function (Blueprint $table) {
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        //     $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        // });

        Schema::create('clientmemberinvites', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('invited_by');
            $table->string('email', 200);
            // $table->string('position', 50);
            $table->string('token', 60);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
        Schema::dropIfExists('client_user');
        Schema::dropIfExists('clientmemberinvites');
    }
}
