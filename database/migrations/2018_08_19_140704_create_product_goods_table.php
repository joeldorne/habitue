<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('product_goods', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('slug', 43)->unique();
        //     $table->string('stripe_product_id')->nullable();

        //     $table->string('productable_type', 50)->nullable();
        //     $table->integer('productable_id')->unsigned();

        //     $table->string('name', 100);
        //     $table->string('description', 250)->nullable();
        //     $table->string('image')->default('storage/products/default.png');

        //     $table->string('currency')->default('usd');
        //     $table->enum('pricing_type', ['singular', 'variation'])->default('singular');

        //     $table->unsignedDecimal('singular_price')->default(1.00);
        //     $table->json('variation_prices')->nullable();

        //     $table->string('activate_form_url')->nullable();

        //     $table->tinyInteger('hide')->default(0);
        //     $table->tinyInteger('disabled')->default(1);
            
        //     $table->softDeletes();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('product_goods');
    }
}
