<?php

namespace App\Http\Middleware;

use Closure;

class NoSubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $subdomain = $route->parameter('subdomain');
        // $route->forgetParameter('subdomain');
        

        // redirect to 'habitue.dev' if entered subdomain is 'www'
        if (trim($subdomain) != '') {

            $domain_tld = config('session.domain_tld');
            $domain_sld = config('session.domain_sld');

            $request->headers->set('host', $domain_tld.'.'.$domain_sld);

            return redirect($request->path());
        }


        return $next($request);
    }
}
