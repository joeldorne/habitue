<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterFromInvite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:100',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|string|confirmed|min:6|max:250'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter your :attribute.',
            'first_name.max' => 'You\'re :attribute must not be more than 100 characters.',
            'email.required' => 'Please enter your :attribute.', 
            'email.max'  => 'That :attribute must not be more than 50 characters.',
            'email.max'  => 'That :attribute is already registered.',
            'password.required'  => 'Please enter your :attribute.',
            'password.confirm'  => 'Please retype your :attribute.',
            'password.min'  => 'You\'re :attribute must be at least 6 characters.',
            'password.max'  => 'You\'re :attribute must not be more than 250 characters.',
        ];
    }
}
