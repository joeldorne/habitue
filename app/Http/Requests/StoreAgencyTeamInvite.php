<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\CheckEmailOnAgencyMembers;
use App\Rules\CheckEmailIfAlreadyInvitedOnAgencyMembers;

class StoreAgencyTeamInvite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required','email', 'max:200', new CheckEmailOnAgencyMembers, new CheckEmailIfAlreadyInvitedOnAgencyMembers],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Please enter your :attribute.', 
            'email.email'  => 'Looks like that is not a valid email address.',
            'email.max'  => 'Email must NOT have more than 200 characters.',
        ];
    }
}
