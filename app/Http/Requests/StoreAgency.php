<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\SubDomainMustNotBeWww;


class StoreAgency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'subdomain' => ['required', 'regex:/^[a-zA-Z]+$/u', 'max:15', 'unique:clients,subdomain', 'unique:agencies,subdomain', new SubDomainMustNotBeWww]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter :attribute of your agency.', 
            'name.max'  => 'The name must not be more than 50 characters.',
            'subdomain.required' => 'You need to assign the :attribute.', 
            'subdomain.regex' => 'The :attribute will only accept letters.', 
            'subdomain.max'  => 'The subdomain must not be more than 15 characters.',
            'subdomain.unique'  => 'The :attribute you entered is already taken.',
        ];
    }
}
