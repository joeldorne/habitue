<?php

namespace App\Http\Requests\ProductCrud;

use Illuminate\Foundation\Http\FormRequest;

class UpdateActivationFormUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activate_form_url' => 'url',
        ];
    }

    public function messages()
    {
        return [
            'activate_form_url.url' => 'Please enter a valid URL.',
        ];
    }
}
