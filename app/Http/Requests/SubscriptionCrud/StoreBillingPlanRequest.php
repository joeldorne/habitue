<?php

namespace App\Http\Requests\SubscriptionCrud;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreBillingPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname' => 'required|max:50',
            'amount' => 'numeric|min:0',
            'interval_count' => 'numeric|min:1',
            'interval' => [Rule::in(['day','week','month','year'])],
            'trial_period_days' => 'numeric|min:0|max:720',

            'max_day' => 'numeric|max:365',
            'max_week' => 'numeric|max:52',
            'max_month' => 'numeric|max:12',
            'max_year' => 'numeric|max:1',
        ];
    }
    
    public function messages()
    {
        return [
            'nickname.required' => 'Please enter the :attribute of the product.', 
            'nickname.max' => 'The :attribute must not be more than 50 characters.',
            
            'amount.numeric' => 'The :attribute must be a number.',
            'amount.min' => 'The :attribute must not be a negative nunmber.',
            
            'interval_count.numeric' => 'The interval period must be a number.',
            'interval_count.min' => 'The interval period must not be at least 1.',
            
            'interval.in' => 'The interval must be either a day, week, month or year.',
            
            'trial_period_days.numeric' => 'The trial period must be a number.',
            'trial_period_days.min' => 'The trial period must not be a negative nunmber.',
            'trial_period_days.max' => 'The trial period must not be more than 720.',

            'max_day.max' => 'The billing interval is invalid. We do not support intervals greater than one year or negative intervals.',
            'max_week.max' => 'The billing interval is invalid. We do not support intervals greater than one year or negative intervals.',
            'max_month.max' => 'The billing interval is invalid. We do not support intervals greater than one year or negative intervals.',
            'max_year.max' => 'The billing interval is invalid. We do not support intervals greater than one year or negative intervals.',
        ];
    }
}
