<?php

namespace App\Http\Requests\SubscriptionCrud;

use Illuminate\Foundation\Http\FormRequest;

class StoreBasicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|unique:product_services',
            'description' => 'max:250',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'Please enter the :attribute of the product.', 
            'name.max' => 'The :attribute must not be more than 100 characters.',
            'name.unique' => 'The product :attribute already exist, please choose a new one.',
            'description.max' => 'The :attribute must not be more than 250 characters.',
        ];
    }
}
