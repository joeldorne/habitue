<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\ProductService;
use App\Subscription;
use Carbon\Carbon;


class MySubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }

    public function index()
    {
        $user = Auth::user();
        $mysubscriptions = $user->activeSubscriptions();
        
        $mysubscriptions->each(function($item, $key) {
            $item->obj_stripe_subs = $this->retrieve_StripeSubscription($item->stripe_id);
            $item->obj_stripe_plan = $this->retrieve_StripePlan($item->stripe_plan);
        });
        
        return view('buyerpages.mysubscription.index', compact('mysubscriptions'));
    }

    public function cancel(Subscription $subscription) 
    {
        Auth::user()->subscription($subscription->name)->cancel();

        return redirect()->route('buyer.mysubscription');
    }

    public function downloadInvoice(Subscription $subscription) 
    {
        $invoices = Auth::user()->invoices();

        dd($invoices);
    }

    protected function retrieve_StripePlan($stripe_plan_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_plan = \Stripe\Plan::retrieve($stripe_plan_id);
        
        return $stripe_plan;
    }

    protected function retrieve_StripeSubscription($stripe_subscription_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

        return $stripe_subscription;
    }

    protected function timestampToEloquentDate($timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
    }

    protected function timestampToCarbon($timestamp)
    {
        return new \Carbon\Carbon(date('Y-m-d H:i:s', $timestamp));
    }
}