<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\ProductService;
use App\Subscription;


class SubscriptionPaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }

    public function checkoutForm(ProductService $subscription, $stripe_plan)
    {
        $plan = $this->retrieve_StripePlan($stripe_plan);

        return view('buyerpages.checkouts.subscription', compact('subscription', 'plan'));
    }

    public function checkout(Request $request)
    {
        $user = Auth::user();
        $stripeToken = $request->input('stripeToken');
        $product_service = ProductService::Slug($request->input('product_service_slug'))->firstOrFail();
        $stripe_product_id = $product_service->stripe_product_id;
        $stripe_plan_id = $request->input('plan_id');

        /**
         * If already subscribed to this subscription return as failed
         */
        if ($user->subscribed($product_service->stripe_product_id)) {
            return view('buyerpages.checkouts.failed');
        }

        // PENDING: CHECK IF THERE IS A EXPIRED OF THE SAME SUBSCRIPTION. DELETE IF THERE IS.

        // $stripe_product = $this->retrieve_StripeProduct($product_service->stripe_product_id);
        $stripe_plan = $this->retrieve_StripePlan($stripe_plan_id);

        // prepare metadata
        $metadata = [
            'user_slug' => $user->slug,
            'user_email' => $user->email,
            'user_name' => $user->fullname,
            'product_service_slug' => $product_service->slug,
        ];

        // include trial period if exists
        if ($stripe_plan->trial_period_days){
            $subscription = $user->newSubscription($stripe_product_id, $stripe_plan_id)->trialDays($stripe_plan->trial_period_days)->create($stripeToken, [
                'metadata' => $metadata
            ]);
        }
        else {
            $subscription = $user->newSubscription($stripe_product_id, $stripe_plan_id)->create($stripeToken, [
                'metadata' => $metadata
            ]);
        }

        $this->storeAdditionalData($subscription->id, $product_service);

        return redirect()->route('buyer.mysubscription');
    }

    /**
     * Store additional data to the Subscription table that laravel-cashier dont handle
     */
    protected function storeAdditionalData($subscription_id, ProductService $product_service)
    {
        $subscription = Subscription::find($subscription_id);

        $subscription->nickname = $product_service->name;
        $subscription->product_service_id = $product_service->id;
        $subscription->save();

        return;
    }

    protected function retrieve_StripeProduct($product_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_product = \Stripe\Product::retrieve($product_id);

        return $stripe_product;
    }

    protected function retrieve_StripePlan($stripe_plan_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_plan = \Stripe\Plan::retrieve($stripe_plan_id);
        
        return $stripe_plan;
    }

    protected function retrieve_StripeSubscription($stripe_subscription_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

        return $stripe_subscription;
    }

    protected function timestampToEloquentDate($timestamp)
    {
        try {
            return date('Y-m-d H:i:s', $timestamp);
        }
        catch(\Exception $e) {
            return null;
        }
    }
}
