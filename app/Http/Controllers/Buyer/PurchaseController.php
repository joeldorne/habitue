<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;


class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }


    public function index()
    {
        //
    }


    public function purchases()
    {
        $purchases = Auth::user()->mypurchases->reverse();
        $purchases->load('product');

        return view('buyerpages.purchases.purchases', compact('purchases'));
    }
}
