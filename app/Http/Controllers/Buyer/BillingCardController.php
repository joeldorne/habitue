<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\StripeCustomer;


class BillingCardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }


    public function cards()
    {
        $customer = Auth::user()->getStripeAPI();

        return view('buyerpages.billings.payment-cards', compact('customer'));
    }


    public function addCard()
    {
        $customer = Auth::user()->getStripeAPI();

        return view('buyerpages.billings.addcard', compact('customer'));
    }


    public function storeCard(Request $request)
    {
        if (!$request->stripeToken || $request->stripeToken == '' || !isset($request->stripeToken))
            return redirect()->back();
        
        $result = Auth::user()->addCard($request->stripeToken);

        return redirect()->route('buyer.billings.cards');
    }


    public function makeDefault($card)
    {
        /**
         * @return bool
         */
        $result = Auth::user()->makeDefaultCard($card);
        
        // FOLLOW UP => if result is false return with failed message. 

        return redirect()->back();
    }


    public function remove($card) 
    {
        /**
         * @return bool
         */
        $result = Auth::user()->removeCard($card);
        
        if (!$result)
            return redirect()->back()->with('error', 'Card cannot be removed!');
        else
            return redirect()->back();
    }
}
