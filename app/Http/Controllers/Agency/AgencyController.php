<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Agency;

use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Agency\Contracts\AgencyRepositoryInterface;

use App\Http\Requests\StoreAgency;
use App\Http\Requests\UpdateAgencyProfile;


class AgencyController extends Controller
{
    protected $agencyRepo;

    public function __construct(AgencyRepositoryInterface $agencyRepo)
    {
        $this->middleware('auth');
        $this->middleware('agency.user', ['except'=>['create','store']]);
        $this->middleware('has.agency', ['except'=>['create','store']]);

        $this->agencyRepo = $agencyRepo;
    }

    public function create() 
    {
        return view('agencypages.starts.create');
    }

    public function store(StoreAgency $request) 
    {
        $data = $request->only(['name','phone','country','subdomain']);
        $data['website_url'] = $request->website;
        $data['created_by'] = $request->user()->id;

        $agency = $this->agencyRepo->createAndJoin($data, $request->user()->id);

        
        event(new \App\Events\NewlyCreatedAgency(Auth::user()));


        return redirect()->route('agency.dashboard');
        // return redirect()->route('agency.dashboard', ['subdomain'=>$agency->subdomain]);
    }

    public function dashboard() 
    {
        return view('agencypages.dashboard');
    }

    public function profile()
    {
        return view('agencypages.profiles.profile');
    }

    public function editprofile()
    {
        $group = Auth::user()->myGroup();

        return view('agencypages.profiles.edit', compact('group'));
    }

    public function updateprofile(UpdateAgencyProfile $request)
    {
        $group = Auth::user()->myAgency();

        $data = $request->only(['name','phone','country','subdomain']);
        $data['website_url'] = $request->website;

        $this->agencyRepo->update($data, $group->id);
        
        return redirect()->route('agency.profile');
    }
}
