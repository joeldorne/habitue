<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;

use App\User;
use App\Agency;
use App\AgencyMemberInvite;

use App\Http\Requests\StoreAgencyTeamInvite;

use App\Mail\AgencyMemberInviteMail;

// use App\Http\Middleware\HasAgency;


class AgencyTeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agency.user');
        $this->middleware('has.agency');
    }

    public function team() 
    {
        return view('agencypages.teams.members');
    }

    public function teamInvites() 
    {
        $invites = AgencyMemberInvite::ForAgency(Auth::user()->agency[0]->id)->get();

        return view('agencypages.teams.invites', compact('invites'));
    }

    public function teamInvitesStore(StoreAgencyTeamInvite $request)
    {
        $invite = AgencyMemberInvite::create([
            'invited_by' => Auth::user()->id,
            'email' => $request->email,
            'agency_id' => Auth::user()->agency[0]->id,
            'token' => $this->generateInviteToken(),
        ]);
        
        // send mail
        Mail::to($invite)->send(new AgencyMemberInviteMail($invite));

        return redirect()->back()->withSuccess('An email have been succesfully sent to invite the new member.');
    }

    public function teamInviteResendEmail($email) 
    {
        // CHECK: ONLY AGENCY: PRIVILEDGE USER CAN RESEND
        $invite = AgencyMemberInvite::WhereEmail($email)->first();
        
        if (!$invite)
            return redirect()->back()->withError('Resend email failed! Cannot find invitation. Please try again.');

        // send mail
        Mail::to($invite)->send(new AgencyMemberInviteMail($invite));

        return redirect()->back()->withSuccess('Invitation email have been resended successfully.');
    }

    public function teamInviteRemove($email) 
    {
        // CHECK: ONLY AGENCY: PRIVILEDGE USER CAN REMOVE
        $invite = AgencyMemberInvite::WhereEmail($email)->first();
        
        if (!$invite)
            return redirect()->back()->withError('Cannot find invitation. Please try again.');

        // remove invitation
        $invite->delete();

        return redirect()->back()->withSuccess("Invitation to `$email` have been removed.");
    }

    public function member(User $member)
    {
        exit('Member Profile Public Page for '.$member->name);
    }

    public function updatePosition(Request $request) 
    {

        // return $request;
        // return Auth::user()->myAgency()->getAdminCount();

        $agency = Agency::find($request->agency_id);

        if (!$agency) {

            return [
                'status' => 'error',
                'message' => 'Page expired! Please refresh the page and try again.'
            ];

        }



        $user = User::Slug($request->user_slug)->first();

        if (!$user) {

            return [
                'status' => 'error',
                'message' => 'Page expired! Please refresh the page and try again.'
            ];

        }



        if ( !in_array($request->position, $agency->getAllowedPositions()) ) {

            return [
                'status' => 'error',
                'message' => 'Invalid Position.'
            ];

        }


        try {

            // saved the current position in case of lossing all admin member
            if ($request->position == 'Member') {

                $prev_position = $agency->members()->find($user->id)->pivot->position;

            }
        

            // update the pivot
            $agency->members()->updateExistingPivot($user->id, ['position'=>$request->position]);


            // check and revert back if theres no admin left present
            if( $request->position == 'Member' && $agency->getAdminCount() < 1) {

                $agency->members()->updateExistingPivot($user->id, ['position'=>$prev_position]);

                return [
                    'status' => 'failed',
                    'message' => 'Failed. Cannot remove the only existing Administrator.'
                ];

            }
            

            return [
                'status' => 'success',
                'message' => 'Position changed.'
            ];

        }
        catch(Exception $e) {

            return [
                'status' => 'error',
                'message' => 'Page expired! Please refresh the page and try again.'
            ];

        }
    }

    // TEMP. TO BE DELETED
    public function teamMemberAdd() 
    {
        // $user = Auth::user();
        // $agency = $user->agency[0];

        // if ($agency->members->contains($invite->user_id))
        // {

        // } 
    }

    // TEMP. TO BE DELETED
    public function teamMemberUpdatePosition() 
    {
        // $agency->members()->updateExistingPivot(3, 
        //     ['position' => 'Developer']
        // );
    }

    protected function generateInviteToken() 
    {
        return str_random(60);
    }
}
