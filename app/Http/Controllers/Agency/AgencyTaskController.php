<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgencyTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agency.user', ['except'=>[]]);
        $this->middleware('has.agency', ['except'=>[]]);
    }
    
    public function index() 
    {
        return view('agencypages.tasks.index');
    }
}
