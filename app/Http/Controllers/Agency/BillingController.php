<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'agency.user', 'has.agency']);
    }


    public function index() 
    {
        return view('agencypages.billings.index');
    }
}
