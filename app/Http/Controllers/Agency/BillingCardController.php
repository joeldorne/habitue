<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\StripeCustomer;


class BillingCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agency.user');
        $this->middleware('has.agency');
    }

    public function cards() 
    {
        $customer = Auth::user()->myGroup()->getStripeAPI();
        
        return view('agencypages.billings.credit-cards', compact('customer'));
    }

    public function addCard()
    {
        $customer = Auth::user()->myGroup()->getStripeAPI();

        return view('agencypages.billings.addcard', compact('customer'));
    }

    public function storeCard(Request $request)
    {
        if (!$request->stripeToken || $request->stripeToken == '' || !isset($request->stripeToken))
            return redirect()->back();
        
        $result = Auth::user()->myGroup()->addCard($request->stripeToken);

        return redirect()->route('agency.billings.cards');
    }

    public function makeDefault($card)
    {
        /**
         * @return bool
         */
        $result = Auth::user()->myGroup()->makeDefaultCard($card);
        
        // FOLLOW UP => if result is false return with failed message. 

        return redirect()->back();
    }

    public function remove($card) 
    {
        /**
         * @return bool
         */
        $result = Auth::user()->myGroup()->removeCard($card);
        
        if (!$result)
            return redirect()->back()->with('error', 'Card cannot be removed!');
        else
            return redirect()->back();
    }
}
