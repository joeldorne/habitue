<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Package;
use App\StripeCustomer;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function payment(Request $request, $slug) 
    {
        $package = Package::slug($slug)->isActive()->isVisible()->first();
        $customer = $this->getStripeCustomerObj();

        return view('payments.purchase', compact('package', 'customer'));
    }

    protected function getStripeCustomerObj() 
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripeCustomer = StripeCustomer::CurrentUser()->first();
        if ($stripeCustomer) 
        {
            try {
                return \Stripe\Customer::retrieve($stripeCustomer->stripe_customer_id);
            }
            catch(\Stripe\Error\Card $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Card-error-msg-';
            } 
            catch (\Stripe\Error\RateLimit $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Ratelimit-error-msg-';
            } 
            catch (\Stripe\Error\InvalidRequest $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // invalid customer id provided
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-invalidRequest-error-msg-';
            } 
            catch (\Stripe\Error\Authentication $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Authentication-error-msg-';
            } 
            catch (\Stripe\Error\ApiConnection $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-ApiConnection-error-msg-';
            } 
            catch (\Stripe\Error\Base $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Base-error-msg-';
            } 
            catch (Exception $e) {
                return $this->stripeErrorRedirect(\Request::route()->getName());
    
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Exception-error-msg-';
            }
        }
        else 
        {
            $customer = \Stripe\Customer::create([
                'email' => Auth::user()->email,
                'description' => '',
                'metadata' =>  array(
                    "user_id" => Auth::user()->id,
                    "first_name" => Auth::user()->first_name,
                    "last_name" => Auth::user()->last_name,
                    "email" => Auth::user()->email,
                    "usertype_id" => Auth::user()->usertype_id,
                    "phone" => Auth::user()->phone,
                ),
            ]);

            $new_stripeCustomer = stripeCustomer::create([
                'user_id' => Auth::user()->id,
                'stripe_customer_id' => $customer->id,
            ]);

            return $customer;
        }
    }

    private function stripeErrorRedirect($route, $messages=null) 
    {
        if (is_array($messages) && !empty($messages)) 
        {
            //
        } 
        else 
        {
            $messages = array('An error occured. Please refresh the page and try again!');
        }

        return redirect()->route($route)->withErrors($messages);
    }
}
