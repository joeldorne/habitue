<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Http\Middleware\Admin;

use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('admin');
        // CREATE MIDDLEWARE FOR AUTH-ADMIN-ONLY-USER
    }

    public function index() 
    {
        return view('admins.index');
    }

    public function clients() 
    {
        //
    }

    public function talents() 
    {
        //
    }
}
