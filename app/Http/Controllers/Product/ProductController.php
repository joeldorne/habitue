<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\Product;
use App\Agency;
use App\Purchase;

use App\StripeCustomer;


class ProductController extends Controller
{
    public function __construct()
    {
        // $this->middleware('nosubdomain')->only('index');
        // $this->middleware('forgetsubdomain')->except('index','products');
        $this->middleware('auth')->only('purchasing');
    }



    // temp method
    public function index() 
    {

        $agencies = Agency::IsVisible()->IsActive()->get();

        $domain_tld = config('session.domain_tld');
        $domain_sld = config('session.domain_sld');

        // dd($agencies);
        foreach ($agencies as $agency) 
        {
            $url = 'http://'.$domain_tld.'.'.$domain_sld.'/p/items/'.$agency->subdomain;
            // $url = 'http://'.$agency->subdomain.'.'.$domain_tld.'.'.$domain_sld.'/products';

            echo "<p>";
            echo '<a href="'.$url.'" target="_blank">'.$url.'</a>';
            echo "</p>";

        }

    }


    public function listOfAgentsPublicLink()
    {
        $agency = Agency::all();

        $agency_with_products = $agency->filter(function($value, $key){
            return $value->products->count();
        });

        echo "<h2>Agencies Product List</h2>";

        foreach ($agency_with_products as $agency) 
        {
            $link = "<a href='".$agency->getPublicMarketUrl()."' target='_blank'> Products</a>";
            
            $html = "<p>---------------------------</p>";
            $html .= "<p>Name: ".$agency->name."</p>";
            $html .= "<p>Product available count: ".$agency->products->count()."</p>";
            $html .= "<p>Link: ".$link."</p>";

            echo $html;
        }
    }



    /**
     * Display all products from the provided [$subdomain]
     * 
     * @param String
     * 
     * @return View
     */
    public function products($subdomain)
    {
        $agency = Agency::WhereSubdomain($subdomain)->firstOrFail();

        return view('publicpages.products.products', compact('agency'));
    }



    public function product(Product $product)
    {
        //
    }



    public function purchasing(Product $product)
    {
        $customer = Auth::user()->myGroup()->getStripeAPI();        

        return view('publicpages.products.purchasing', compact('product', 'customer'));
    }



    public function purchased(Request $request)
    {
        $product = Product::Slug($request->product)->first();
        
        $price = $product->getPrice($request->price_name);

        $payment_status = Auth::user()->myGroup()->payUsingExistingCard($request->card, $product, $price); //customer_id, charge_id

        
        $purchase = Purchase::create([
            'client_id' => Auth::user()->myGroup()->id,
            'agency_id' => $product->agency_id,
            'product_id' => $product->id,
            'price' => $price,
            'subscription' => $product->subscription,
            'customer_id' => $payment_status['customer_id'],
            'charge_id' => $payment_status['charge_id'],
        ]);

        
        return redirect()->route('client.purchases');
        // return redirect()->route('client.purchases', Auth::user()->myClient()->subdomain);
    }
}
