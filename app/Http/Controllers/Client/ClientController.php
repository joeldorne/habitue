<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Client;

use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\Contracts\ClientRepositoryInterface;

use App\Http\Requests\StoreClient;
use App\Http\Requests\UpdateClientProfile;


class ClientController extends Controller
{
    protected $clientRepo;

    public function __construct(ClientRepositoryInterface $clientRepo)
    {
        $this->middleware('auth');
        $this->middleware('client.user', ['except'=>['create','store']]);
        $this->middleware('has.client', ['except'=>['create','store']]);

        $this->clientRepo = $clientRepo;
    }

    public function create() 
    {
        return view('clientpages.starts.create');
    }

    public function store(StoreClient $request)
    {
        $data = $request->only(['name','phone','country','subdomain']);
        $data['website_url'] = $request->website;
        $data['created_by'] = $request->user()->id;

        $this->clientRepo->createAndJoin($data, $request->user()->id);

        
        event(new \App\Events\NewlyCreatedClient($request->user()));


        return redirect()->route('client.dashboard');
        // return redirect()->route('client.dashboard', ['subdomain'=>$client->subdomain]);
    }

    public function dashboard() 
    {
        return view('clientpages.dashboard');
    }

    public function profile()
    {
        return view('clientpages.profiles.profile');
    }

    public function editprofile()
    {
        $group = Auth::user()->myClient();

        return view('clientpages.profiles.edit', compact('group'));
    }

    public function updateprofile(UpdateClientProfile $request)
    {
        $group = Auth::user()->myClient();

        $data = $request->only(['name','phone','country','subdomain']);
        $data['website_url'] = $request->website;

        $this->clientRepo->update($data, $group->id);
        
        return redirect()->route('client.profile');
    }
}
