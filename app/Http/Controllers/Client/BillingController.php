<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('client.user');
        $this->middleware('has.client');
    }

    public function index() 
    {
        return view('clientpages.billings.index');
    }

    public function pending()
    {
        return view('clientpages.billings.pending');
    }
}