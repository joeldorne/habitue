<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() 
    {
        // check for user role
        if (Auth::user()->hasRole('admin')) {
            exit('billings for admin');
        }

        if(Auth::user()->hasRole('buyer')) {
            return $this->handleBuyer();
        }

        if (Auth::user()->hasRole('agency')) {
            return $this->handleAgency();
        }
        
        if (Auth::user()->hasRole('client')) {
            return $this->handleClient();
        }
    }

    public function cards() 
    {
        // check for user role
        if (Auth::user()->hasRole('admin')) {
            exit('billings for admin');
        }

        if (Auth::user()->hasRole('agency')) {
            return redirect()->route('agency.billings.cards');
        }
        
        if (Auth::user()->hasRole('client')) {
            return redirect()->route('client.billings.cards');
        }
    }




    public function handleAdmin()
    {
        exit('billings for admin');
    }

    public function handleBuyer()
    {
        return redirect()->route('buyer.billings');
    }

    public function handleAgency()
    {
        return redirect()->route('agency.billings');
        // return redirect()->route('agency.billings', ['subdomain'=>Auth::user()->agency[0]->subdomain]);
    }

    public function handleClient()
    {        
        if (Auth::user()->myGroup()->stripeCustomer == null)
        {            
            /**
             * create Stripe Account for the client group
             * 
             * @return bool 
             */
            $create_stripe_account_status = Auth::user()->getClient()->getStripeAPI();
        }

        return redirect()->route('client.billings.pending');
        // return redirect()->route('client.billings.pending', ['subdomain'=>Auth::user()->client[0]->subdomain]);
    }
}