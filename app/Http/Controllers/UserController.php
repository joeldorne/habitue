<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;

use App\Repositories\User\Contracts\UserRepositoryInterface;
use App\Repositories\User\UserRepository;

use App\Http\Requests\UpdateUserProfile;
use App\Http\Requests\UpdateUserPassword;


class UserController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->middleware('auth');

        $this->userRepo = $userRepo;
    }


    public function profile()
    {
        return view('userpages.profiles.profile');
    }


    public function edit() 
    {
        return view('userpages.profiles.edit');
    }


    public function changepassword()
    {
        return view('userpages.profiles.changepassword');
    }


    public function update(UpdateUserProfile $request)
    {
        $this->userRepo->update(
            $request->only([
                'first_name',
                'last_name']
            ), 
            // Auth::user()->id
            $request->user()->id
        );

        return redirect()->route('user.profile');
    }


    public function updatepassword(UpdateUserPassword $request)
    {
        $this->userRepo->update(
            $request->only(['password']), 
            // Auth::user()->id
            $request->user()->id
        );

        return redirect()->route('user.profile');
    }
}
