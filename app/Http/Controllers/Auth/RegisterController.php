<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Repositories\User\UserRepository;
use App\Repositories\User\Contracts\UserRepositoryInterface;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    // protected $redirectToIfInvalidUrl = '/register?role=agency';

    protected $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->middleware('guest');

        $this->userRepo = $userRepo;
    }

    // public function showRegistrationForm()
    // {
    //     return view('auth.register');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:100',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {        
        $data = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'role_id' => 4, // role=buyer 
        ];
        
        $user = $this->userRepo->create($data);

        event(new \App\Events\UserRegistration($user));

        return $user;
    }


    /**
     * Old Registration Form For Agency And Client Users
     */
    public function showRegistrationFormWithGroup()
    {
        if( empty(\Request::get('role')) )
            return redirect($this->redirectToIfInvalidUrl);

        $role = \Request::get('role');

        if( !in_array($role, $this->userRepo->allowed_roles) )
            return abort(404);

        return view('auth.register', compact('role'));
    }


    /**
     * Old Registration Store For Agency And Client Users
     */
    protected function createWithGroup(array $data)
    {
        if ( empty($data['role']) )
            return abort(404);

        if( !in_array($data['role'], $this->userRepo->allowed_roles) )
            return abort(404);
        
        if ($data['role'] == 'agency')
            $role_id = 2;
        else
            $role_id = 3;

        
        $data = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'role_id' => $role_id, 
        ];
        
        $user = $this->userRepo->create($data);

        event(new \App\Events\UserRegistration($user));

        return $user;
    }
}
