<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Purchase;


class PaymentReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    public function index()
    {
        return view('adminpages.paymentreports.index');
    }


    public function sales()
    {
        $purchases = Purchase::with(['purchasable', 'product'])->get()->reverse();
        
        return view('adminpages.paymentreports.sales', compact('purchases'));

    }


    public function updateCompletion(Request $request) 
    {
        $purchase = Purchase::Slug($request->purchase_slug)->firstOrFail();

        if (! $purchase) {

            return [
                'status' => 'error',
                'message' => 'Invalid data. Please refresh the page and try again.',
            ];

        }

        $purchase->completion = $request->completion;
        $purchase->save();

        return [
            'status' => 'success',
            'message' => 'Updated.',
        ];
    }

    
    public function updateStatus(Request $request)
    {
        $purchase = Purchase::Slug($request->purchase_slug)->firstOrFail();

        if (! $purchase) {

            return [
                'status' => 'error',
                'message' => 'Invalid data. Please refresh the page and try again.',
            ];

        }

        $purchase->status = $request->status;
        $purchase->save();

        return [
            'status' => 'success',
            'message' => 'Updated.',
        ];
    }

    public function test1()
    {
        $div = "<div id='title'></di>";
        $html = $div.$div.$div;

        echo $html;
    }
}
