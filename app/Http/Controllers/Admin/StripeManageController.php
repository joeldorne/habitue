<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\ProductService;

use App\Repositories\Subscription\Contracts\SubscriptionRepositoryInterface;

class StripeManageController extends Controller
{
    public function __construct(SubscriptionRepositoryInterface $subscriptionRepo)
    {
        $this->middleware(['auth','admin']);

        $this->subscriptionRepo = $subscriptionRepo;

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function migration() 
    {
        return view('adminpages.settings.stripes.migration');
    }

    public function migrateProductService()
    {
        // delete all existing product service from DB table
        $this->truncateProductServiceTable();

        $options = [
            "limit" => 9999, 
            "active" => true,
        ];

        $stripe_product_service = $this->retrieve_StripeProductService($options);

        foreach ($stripe_product_service->data as $item) {
            $data = [
                'stripe_product_id' => $item->id,
                'name' => $item->name,
                'description' => $item->metadata['product_desc'],
                'disabled' => 0
            ];

            $this->store($data);
        }

        return redirect()->route('admin.settings.stripe.migration');
    }

    protected function truncateProductServiceTable() 
    {
        ProductService::truncate();
    }

    protected function store($data) 
    {
        // create an unsaved model for new subscription 
        $subscription_model = $this->subscriptionRepo->new($data);

        // save the new subscription model via User relational method
        return Auth::user()->product_services()->save($subscription_model);
    }

    protected function retrieve_StripePlan($stripe_plan_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_plan = \Stripe\Plan::retrieve($stripe_plan_id);
        
        return $stripe_plan;
    }

    protected function retrieve_StripeSubscription($stripe_subscription_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

        return $stripe_subscription;
    }
    
    protected function retrieve_StripeProductService($options)
    {
        return \Stripe\Product::all($options);
    }
}
