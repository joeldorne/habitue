<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Payment;
use App\Package;
use App\MyPackage;
use App\MySubscription;
use App\StripeCustomer;

class StripeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function checkout(Request $request) 
    {
        $package = Package::slug($request->package)->isActive()->isVisible()->first();

        try {
            /*
             * charge using the customer object
             */
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

            $token = $request->stripeToken;

            // Charge the user's card:
            $charge = \Stripe\Charge::create(array(
                "amount" => ($package->price * 100),
                "currency" => "usd",
                "description" => $package->display_name,
                "statement_descriptor" => "Custom descriptor", // Statement descriptors are limited to 22 characters, cannot use the special characters <, >, ', or ", and must not consist solely of numbers.
                // "customer" => $customer->id,
                "source" => $token,
                // "capture" => false,
                "metadata" => [
                    "user_id" => Auth::user()->id,
                    "user_email" => Auth::user()->email,
                ]
            ));
            // dd($charge);
            
            $customer_id = (isset($customer)) ? $customer->id : 0;

            return $this->storePaymentToDB($package, $customer_id, $charge->id);
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('packages');
    
            // card cant find from the customer's cards
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
    }

    public function payWithCustomerCard($package, $card) 
    {
        $package = Package::slug($package)->isActive()->isVisible()->first();

        /* Get Customer Object from Stripe server
         * Customer Object for the current user
         */
        $customer = $this->getStripeCustomerObj();
        if ( !isset($customer->object) )
            return $this->stripeErrorRedirect('packages');

            $card = $this->getStripeCustomerCardObj($customer, $card);

        try {
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

            // Charge the user's card:
            $charge = \Stripe\Charge::create(array(
                "amount" => ($package->price * 100),
                "currency" => "usd",
                "description" => $package->display_name,
                "statement_descriptor" => "Custom descriptor", // Statement descriptors are limited to 22 characters, cannot use the special characters <, >, ', or ", and must not consist solely of numbers.
                "customer" => $customer->id,
                "source" => $card,
                // "capture" => false,
                "metadata" => [
                    "user_id" => Auth::user()->id,
                    "user_email" => Auth::user()->email,
                ]
            ));
            
            $customer_id = (isset($customer)) ? $customer->id : 0;

            return $this->storePaymentToDB($package, $customer_id, $charge->id);
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('packages');
    
            // card cant find from the customer's cards
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('packages');
    
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
    }

    protected function storePaymentToDB(Package $package, $customer_id, $charge_id) 
    {
        $payment = Payment::create([
            'user_id' => Auth::user()->id,
            'package_id' =>  $package->id,
            'slug' => $this->generateUniquePaymentSlug(),
            'currency' => $package->currency,
            'total_amount' => $package->price,
            'stripe_customer_id' => $customer_id,
            'stripe_charge_id' => $charge_id,
        ]);
        
        if ($package->subscription == 0)
        {
            return $this->storeMyPackageToDB($package, $payment->id);
        }
        else
        {
            return $this->storeMySubscriptionToDB($package, $payment->id);
        }
    }

    public function storeMyPackageToDB(Package $package, $payment_id) 
    {
        $mypackage = MyPackage::create([
            'package_id' =>  $package->id,
            'user_id' => Auth::user()->id,
            'payment_id' => $payment_id,
            'slug' => $this->generateUniqueMyPackageSlug(),
            'name' => $package->name,
            'display_name' => $package->display_name,
            'description' => $package->description,
            'currency' => $package->currency,
            'price' => $package->price,
            'package_category_id' => $package->package_category_id,
        ]);
        
        if($mypackage)
            return redirect()->route('mypackages');
        else
            return redirect()->route('packages');//with failed message           
    }

    public function storeMySubscriptionToDB(Package $package, $payment_id) 
    {
        $mysubscription = MySubscription::create([
            'package_id' =>  $package->id,
            'user_id' => Auth::user()->id,
            'payment_id' => $payment_id,
            'slug' => $this->generateUniqueMySubscriptionSlug(),
            'name' => $package->name,
            'display_name' => $package->display_name,
            'description' => $package->description,
            'currency' => $package->currency,
            'price' => $package->price,
            'package_category_id' => $package->package_category_id,
        ]);
        
        if($mysubscription)
            return redirect()->route('mysubscriptions');
        else
            return redirect()->route('packages');//with failed message  
    }

    protected function getStripeCustomerObj() 
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripeCustomer = StripeCustomer::CurrentUser()->first();
        if ($stripeCustomer) 
        {
            try {
                $customer = \Stripe\Customer::retrieve($stripeCustomer->stripe_customer_id);

                if ($customer->object != 'customer')
                    return $this->stripeErrorRedirect('packages');
                
                return $customer;
            }
            catch(\Stripe\Error\Card $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Card-error-msg-';
            } 
            catch (\Stripe\Error\RateLimit $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Ratelimit-error-msg-';
            } 
            catch (\Stripe\Error\InvalidRequest $e) {
                return $this->stripeErrorRedirect('packages');
    
                // invalid customer id provided
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-invalidRequest-error-msg-';
            } 
            catch (\Stripe\Error\Authentication $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Authentication-error-msg-';
            } 
            catch (\Stripe\Error\ApiConnection $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-ApiConnection-error-msg-';
            } 
            catch (\Stripe\Error\Base $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Base-error-msg-';
            } 
            catch (Exception $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Exception-error-msg-';
            }
        }
        else 
        {
            $customer = \Stripe\Customer::create([
                'email' => Auth::user()->email,
                'description' => '',
                'metadata' =>  array(
                    "user_id" => Auth::user()->id,
                    "first_name" => Auth::user()->first_name,
                    "last_name" => Auth::user()->last_name,
                    "email" => Auth::user()->email,
                    "usertype_id" => Auth::user()->usertype_id,
                    "phone" => Auth::user()->phone,
                ),
            ]);

            $new_stripeCustomer = stripeCustomer::create([
                'user_id' => Auth::user()->id,
                'stripe_customer_id' => $customer->id,
            ]);

            return $customer;
        }
    }

    protected function getStripeCustomerCardObj(\Stripe\Customer $customer, $card_id) 
    {
        try {
            return $customer->sources->retrieve($card_id);
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('packages');

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('packages');

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('packages');

            // invalid customer id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('packages');

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('packages');

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('packages');

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('packages');

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
    }

    public function generateUniquePaymentSlug() 
    {
        do {
            $slug = 'pm_'.str_random(10);
        } while(Payment::Slug($slug)->exists());

        return $slug;
    }

    public function generateUniqueMyPackageSlug() 
    {
        do {
            $slug = 'mp_'.str_random(10);
        } while(MyPackage::Slug($slug)->exists());

        return $slug;
    }

    public function generateUniqueMySubscriptionSlug() 
    {
        do {
            $slug = 'ms_'.str_random(10);
        } while(MySubscription::Slug($slug)->exists());

        return $slug;
    }

    private function stripeErrorRedirect($route, $messages=null) 
    {
        if (is_array($messages) && !empty($messages)) 
        {
            //
        } 
        else 
        {
            $messages = array('Sorry, something went wrong. Please refresh the page and try again!');
        }

        return redirect()->route($route)->withErrors($messages);
    }
}
