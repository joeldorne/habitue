<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

// use App\Agency;
// use App\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function guest() 
    {
        return view('welcome');
    }

    public function index() 
    {
        if (!Auth::check())
            return redirect()->route('guest');
        
        $user = Auth::user();

        // check for user role
        if ($user->hasRole('admin')) {
            return $this->handleAdmin();
        }

        if ($user->hasRole('buyer')) {
            return $this->handleBuyer();
        }

        if ($user->hasRole('agency')) {
            return $this->handleAgency();
        }
        
        if ($user->hasRole('client')) {
            return $this->handleClient();
        }
        
        Auth::logout();
        return redirect()->route('guest');
    }

    private function handleAdmin() 
    {
        return redirect('/admin');
    }

    private function handleBuyer()
    {
        return redirect()->route('buyer.dashboard');
    }

    private function handleAgency() 
    {
        $user = Auth::user();

        // find agency if exists
        if ($user->agency->count()) 
            return redirect()->route('agency.dashboard');
        // return redirect()->route('agency.dashboard', ['subdomain'=>$user->agency[0]->subdomain]);

        // if not on agency, check for agency invitation
        //

        // if not on agency and not on invitation, redirect to create agency
        return redirect()->route('agency.start');
    }

    private function handleClient() 
    {
        $user = Auth::user();
        
        // find client if exists
        if ($user->client->count()) 
            return redirect()->route('client.dashboard');
        // return redirect()->route('client.dashboard', ['subdomain'=>$user->client[0]->subdomain]);

        // if not on client and not on invitation, redirect to create client
        return redirect()->route('client.start');
    }
}
