<?php

/**
 * Set menu class mainly if `active` or not
 * 
 * @param
 * 1-the current route provide by laravel helper
 * 2-the route assign for the given menu
 * 3-the class to be assign if the current-route == assign-route
 * 
 * @return
 * assigned class OR empty
 */
function toggleClass($currentRoute, $thisClass, $class) 
{
    if ($currentRoute == $thisClass) 
    {
        return $class;
    }
    return '';
}


function get_data_from_variation_prices($variation_prices, $data = 'amount')
{
    if (trim($variation_prices) == '') {

        return '';
    }


    $json = json_decode($variation_prices);

    $html = '';
    foreach($json as &$price) {
        
        $html .= "$ ".$price->$data. ", ";
    }

    return $html;
}

function timestampToEloquentDate($timestamp)
{
    return date('Y-m-d H:i:s', $timestamp);
}

function timestampToCarbon($timestamp)
{
    return new \Carbon\Carbon(date('Y-m-d H:i:s', $timestamp));
}
