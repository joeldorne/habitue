<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientMemberInvite extends Model
{
    protected $table = 'clientmemberinvites';
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'invited_by',
        'email',
        'client_id',
        'token',
    ];

    public function client() 
    {
        return $this->belongsTo('App\Client');
    }

    public function scopeForClient($query, $client_id)
    {
        return $query->where('client_id', '=', $client_id);
    }

    public function scopeWhereEmail($query, $email)
    {
        return $query->where('email', '=', $email);
    }

    public function scopeWhereToken($query, $token)
    {
        return $query->where('token', '=', $token);
    }

    public static function create(array $data)
    {
        $data['token'] = ClientMemberInvite::generateToken();

        $model = static::query()->create($data);

        return $model;
    }

    public static function generateToken() 
    {
        do {
            $token = str_random(60);
        } while(ClientMemberInvite::WhereToken($token)->exists());

        return $token;
    }
}
