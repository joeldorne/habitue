<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\StatusVisibility;
use App\Traits\StatusActivity;
use App\Traits\StripeAPIable;
use App\Traits\ProductSalable;


class Agency extends Model
{
    use StatusVisibility, StatusActivity, StripeAPIable, ProductSalable;



    protected $table = "agencies";
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $fillable = [
        'created_by',
        'slug',
        'name',
        'phone',
        'website_url',
        'country',
        'subdomain',
        'hide',
        'disabled',
    ];
    protected $hidden = ['id', 'created_by'];

    
    
    private $allowedPositions = ['Administrator', 'Member'];



    public function members() 
    {
        return $this->belongsToMany('App\User')->withPivot('position')->withTimestamps();
    }

    public function memberInvites() 
    {
        return $this->hasMany('App\AgencyMemberInvite');
    }

    // public function stripeCustomer() 
    // {
    //     return $this->morphOne('App\StripeCustomer', 'stripecustomerable');
    // }

    // public function products()
    // {
    //     return $this->hasMany('App\Product');
    // }

    // public function public_products() 
    // {
    //     return $this->hasMany('App\Product')->PublicMarket();
    // }

    // public function product_sales()
    // {
    //     return $this->hasMany('App\Purchase')->with('product', 'buyer');
    // }





    public function getAgencyPositionAttribute($value)
    {
        return ucwords($this->pivot->position);
    }





    /**
     * Append where query for the slug
     * 
     * @return QueryBuilder
     */
    public function scopeWhereSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeWhereSubdomain($query, $subdomain) 
    {
        return $query->where('subdomain', '=', $subdomain);
    }



    public function isAdministrator()
    {
        if ($this->pivot->position == 'Administrator')
        {
            return true;
        }

        return false;
    }

    public function isMember()
    {
        if ($this->pivot->position == 'Member')
        {
            return true;
        }

        return false;
    }

    public function getAdminCount() 
    {
        return $this->members()->where('position', 'Administrator')->count();
    }

    public function getAllowedPositions() 
    {
        return $this->allowedPositions;
    }
}