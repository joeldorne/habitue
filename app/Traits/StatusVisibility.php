<?php

namespace App\Traits;

trait StatusVisibility
{
    protected $STATUS_VISIBLE = false;
    protected $STATUS_HIDDEN = true;

    public function scopeIsVisible($query) 
    {
        return $query->where('hide', '=', $this->STATUS_VISIBLE);
    }

    public function scopeIsHidden($query) 
    {
        return $query->where('hide', '=', $this->STATUS_HIDDEN);
    }
}