<?php

namespace App\Traits;


Trait ProductSalable
{
    /**
     * Get all products from this model
     */
    public function myproducts() 
    {
        return $this->morphMany('App\Product', 'productable');
    }



    public function getTotalSales()
    {
        $total = 0.00;

        foreach ($this->product_sales as $sale) {

            $total += (float) $sale->price;

        }

        return $total;
    }

    public function getSaleCount()
    {
        return $this->product_sales->count();
    }



    // public function products()
    // {
    //     return $this->hasMany('App\Product');
    // }

    // public function public_products() 
    // {
    //     return $this->hasMany('App\Product')->PublicMarket();
    // }

    // public function product_sales()
    // {
    //     return $this->hasMany('App\Purchase')->with('product', 'buyer');
    // }
}
