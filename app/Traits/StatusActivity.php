<?php

namespace App\Traits;

trait StatusActivity
{
    protected $STATUS_ACTIVE = false;
    protected $STATUS_DISABLED = true;

    public function scopeIsActive($query) 
    {
        return $query->where('disabled', '=', $this->STATUS_VISIBLE);
    }

    public function scopeIsDisabled($query) 
    {
        return $query->where('disabled', '=', $this->STATUS_DISABLED);
    }
}