<?php

namespace App\Traits;

trait UserAttribute
{
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getFirstNameAttribute($value) 
    {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value) 
    {
        return ucfirst($value);
    }

    public function getEmailAttribute($value) 
    {
        return strtolower($value);
    }

    public function getFullNameAttribute()
    {
        return ucwords("{$this->first_name} {$this->last_name}");
    }
}