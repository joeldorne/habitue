<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\User\Contracts\UserRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\Agency\Contracts\AgencyRepositoryInterface;
use App\Repositories\Agency\AgencyRepository;
use App\Repositories\Client\Contracts\ClientRepositoryInterface;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Product\Contracts\ProductRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Subscription\Contracts\SubscriptionRepositoryInterface;
use App\Repositories\Subscription\SubscriptionRepository;


class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        $this->app->bind(AgencyRepositoryInterface::class, AgencyRepository::class);

        $this->app->bind(ClientRepositoryInterface::class, ClientRepository::class);

        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);

        $this->app->bind(SubscriptionRepositoryInterface::class, SubscriptionRepository::class);
    }
}