<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\StatusVisibility;
use App\Traits\StatusActivity;

class Purchase extends Model
{
    use StatusVisibility, StatusActivity; 

    protected $table = "purchases";
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'slug',

        'product_id',
        'name',
        'price',

        'activate_form_url',

        'stripe_id',
        'charge_id',

        'status',
        'completion',

        'hide',
        'disabled',
    ];
    protected $hidden = ['id'];



    public function purchasable()
    {
        return $this->morphTo();
    }


    public function product()
    {
        return $this->belongsTo('App\Product');
    }


    // public function agency() 
    // {
    //     return $this->belongsTo('App\Agency');
    // }


    // public function buyer() 
    // {
    //     return $this->belongsTo('App\Client', 'client_id');
    // }


    // public function seller() 
    // {
    //     return $this->belongsTo('App\Agency', 'agency_id');
    // }





    public function scopeWhereSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }





    // public static function create(array $data)
    // {
    //     $data['slug'] = Purchase::generateSlug();

    //     $model = static::query()->create($data);

    //     return $model;
    // }

    public static function generateSlug() 
    {
        do {
            $slug = 'pc_'.str_random(30);
        } while(Purchase::WhereSlug($slug)->exists());

        return $slug;
    }
}