<?php

namespace App\Listeners;

use App\Events\NewlyCreatedAgency;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\CongratsNewlyCreatedAgency;


class SendCongratsNewlyCreatedAgencyMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewlyCreatedAgency  $event
     * @return void
     */
    public function handle(NewlyCreatedAgency $event)
    {
        Mail::to($event->user)->send(new CongratsNewlyCreatedAgency($event->user));
    }
}
