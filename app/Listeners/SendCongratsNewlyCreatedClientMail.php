<?php

namespace App\Listeners;

use App\Events\NewlyCreatedClient;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\CongratsNewlyCreatedClient;

class SendCongratsNewlyCreatedClientMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewlyCreatedClient  $event
     * @return void
     */
    public function handle(NewlyCreatedClient $event)
    {
        Mail::to($event->user)->send(new CongratsNewlyCreatedClient($event->user));
    }
}
