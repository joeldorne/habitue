<?php

namespace App\Repositories\Agency;

use App\Agency;
use App\Repositories\BaseRepository;
use App\Repositories\Agency\Contracts\AgencyRepositoryInterface;
use App\Repositories\Agency\Exceptions\CreateAgencyErrorException;
use App\Repositories\Agency\Exceptions\AgencyNotFoundException;
use App\Repositories\Agency\Exceptions\UpdateAgencyErrorException;
use App\Repositories\Agency\Exceptions\AgencyMemberAttachErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ErrorException;
use Exception;


class AgencyRepository extends BaseRepository implements AgencyRepositoryInterface
{
    protected $model;

    private static $slug_prefix = 'ag_';

    /**
     * AgencyRepository constructor
     * @param Agency $agency
     */
    public function __construct(Agency $agency)
    {
        $this->model = $agency;
    }


    /**
     * @return Collection of Agency
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        $this->model->all();
    }


    /**
     * @param array $data
     * @return Agency
     * @throws CreateAgencyErrorException
     */
    public function create(array $data) : Agency
    {
        $data['slug'] = $this->generateSlug();

        try {
            return $this->model = $this->model->create($data);
        } catch (ErrorException $e) {
            throw new CreateAgencyErrorException($e);
        }
    }


    /**
     * @param array $data
     * @param int $user_id
     * @return Agency
     * @throws CreateAgencyErrorException
     */
    public function createAndJoin(array $data, $user_id) : Agency
    {
        $data['slug'] = $this->generateSlug();

        try {
            $this->model = $this->model->create($data);
        } catch (ErrorException $e) {
            throw new CreateAgencyErrorException($e);
        }

        $this->addMember($this->model, $user_id, 'Administrator');

        return $this->model;
    }


    /**
     * @param int $id
     * @return Agency
     * @throws AgencyNotFoundException
     */
    public function find(int $id) : Agency
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new AgencyNotFoundException($e);
        }
    }


    /** 
     * @param array $data
     * @return bool
     * @throws UpdateAgencyErrorException
     */
    public function update(array $data, $id) : bool
    {
        try {
            return $this->model->findOrFail($id)->update($data);
        } catch(Exception $e) {
            throw new UpdateAgencyErrorException($e);
        } catch(QueryException $e) {
            throw new UpdateAgencyErrorException($e);
        }
    }


    /** 
     * @param array $data
     * @param integer $id
     * @return bool
     * @throws UpdateAgencyErrorException
     */
    public function updateById(array $data, $id) : bool
    {
        $agency = $this->find($id);

        try {
            return $agency->update($data);
        } catch(\Exception $e) {
            throw new UpdateAgencyErrorException($e);
        }
    }


    /**
     * @return bool
     */
    public function delete($id) : bool
    {
        return $this->model->find($id)->delete();
    }


    public function show($id) : Agency
    {
        return $this->model->findOrFail($id);
    }


    public function getModel()
    {
        return $this->model;
    }


    public function setModel(Agency $model) : AgencyRepository
    {
        $this->model = $model;

        return $this;
    }


    public function with($relations)
    {
        return $this->model->with($relations);
    }
    

    /**
     * Generate a random string that will serve as the slug for the agency
     * Using a prefix to identify as slug for Agency
     * 
     * @return String slug
     */
    public static function generateSlug() : String
    {
        do {
            $slug = self::$slug_prefix.str_random(20);
        } while(Agency::WhereSlug($slug)->exists());

        return $slug;
    }


    /**
     * Add User as member to the agency
     * @param Agency $agency
     * @param int $user_id
     * @return null
     */
    public function addMember(Agency $agency, int $user_id, $position='Member')
    {
        try {
            return $agency->members()->attach($user_id, ['position'=>$position]);
        }
        catch(\Exception $e){
            throw new AgencyMemberAttachErrorException($e);
        }
    }
}