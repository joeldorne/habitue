<?php

namespace App\Repositories\Client;

use App\Client;
use App\Repositories\BaseRepository;
use App\Repositories\Client\Contracts\ClientRepositoryInterface;
use App\Repositories\Client\Exceptions\CreateClientErrorException;
use App\Repositories\Client\Exceptions\ClientNotFoundException;
use App\Repositories\Client\Exceptions\UpdateClientErrorException;
use App\Repositories\Client\Exceptions\ClientMemberAttachErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ErrorException;
use Exception;


class ClientRepository extends BaseRepository implements ClientRepositoryInterface
{
    protected $model;

    private static $slug_prefix = 'cl_';

    /**
     * ClientRepository constructor
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->model = $client;
    }


    /**
     * @return Collection of Client
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        $this->model->all();
    }


    /**
     * @param array $data
     * @return Client
     * @throws CreateClientErrorException
     */
    public function create(array $data) : Client
    {
        $data['slug'] = $this->generateSlug();

        try {
            return $this->model = $this->model->create($data);
        } catch (ErrorException $e) {
            throw new CreateClientErrorException($e);
        }
    }


    /**
     * @param array $data
     * @param int $user_id
     * @return Client
     * @throws CreateClientErrorException
     */
    public function createAndJoin(array $data, $user_id) : Client
    {
        $data['slug'] = $this->generateSlug();

        try {
            $this->model = $this->model->create($data);
        } catch (ErrorException $e) {
            throw new CreateClientErrorException($e);
        }

        $this->addMember($this->model, $user_id, 'Administrator');

        return $this->model;
    }


    /**
     * @param int $id
     * @return Client
     * @throws ClientNotFoundException
     */
    public function find(int $id) : Client
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ClientNotFoundException($e);
        }
    }


    /** 
     * @param array $data
     * @return bool
     * @throws UpdateClientErrorException
     */
    public function update(array $data, $id) : bool
    {
        try {
            return $this->model->findOrFail($id)->update($data);
        } catch(Exception $e) {
            throw new UpdateClientErrorException($e);
        } catch(QueryException $e) {
            throw new UpdateClientErrorException($e);
        }
    }


    /** 
     * @param array $data
     * @param integer $id
     * @return bool
     * @throws UpdateClientErrorException
     */
    public function updateById(array $data, $id) : bool
    {
        $client = $this->find($id);

        try {
            return $client->update($data);
        } catch(\Exception $e) {
            throw new UpdateClientErrorException($e);
        }
    }


    /**
     * @return bool
     */
    public function delete($id) : bool
    {
        return $this->model->find($id)->delete();
    }


    public function show($id) : Client
    {
        return $this->model->findOrFail($id);
    }


    public function getModel()
    {
        return $this->model;
    }


    public function setModel(Client $model) : ClientRepository
    {
        $this->model = $model;

        return $this;
    }


    public function with($relations)
    {
        return $this->model->with($relations);
    }
    

    /**
     * Generate a random string that will serve as the slug for the client
     * Using a prefix to identify as slug for Client
     * 
     * @return String slug
     */
    public static function generateSlug() : String
    {
        do {
            $slug = self::$slug_prefix.str_random(20);
        } while(Client::WhereSlug($slug)->exists());

        return $slug;
    }


    /**
     * Add User as member to the client
     * @param Client $client
     * @param int $user_id
     * @return null
     */
    public function addMember(Client $client, int $user_id, string $position='Member')
    {
        try {
            return $client->members()->attach($user_id, ['position'=>$position]);
        }
        catch(\Exception $e){
            throw new ClientMemberAttachErrorException($e);
        }
    }
}