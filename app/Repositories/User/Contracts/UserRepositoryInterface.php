<?php 

namespace App\Repositories\User\Contracts;

use App\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    // public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'desc');

    // public function create(array $data);

    // public function update(array $data, $id);

    // public function delete($id);

    // public function show($id);
}