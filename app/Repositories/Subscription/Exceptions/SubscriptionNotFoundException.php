<?php

namespace App\Repositories\Subscription\Exceptions;

class SubscriptionNotFoundException extends \Exception
{
    //
}