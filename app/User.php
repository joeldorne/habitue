<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\UserAttribute;
use App\Traits\StatusVisibility;
use App\Traits\StatusActivity;
use App\Traits\AgencyGroup;
use App\Traits\ClientGroup;
use App\Traits\ProductSalable;
use App\Traits\StripeAPIable;
use App\Traits\Purchasable;

use Laravel\Cashier\Billable;


class User extends \TCG\Voyager\Models\User
{
    use Notifiable, 
        SoftDeletes, 
        UserAttribute, 
        StatusVisibility, 
        StatusActivity,
        // AgencyGroup,
        // ClientGroup,
        ProductSalable,
        Billable,
        // StripeAPIable,
        Purchasable;
    
    
        
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'trial_ends_at'];
    protected $hidden = [
        'id', 
        'role_id', 
        'password', 
        'remember_token', 
        'created_at', 
        'updated_at', 
        'activation_token', 
        'hide', 
        'disabled', 
        'deleted_at', 
        'stripe_id', 
        'card_brand', 
        'card_last_four', 
        'trial_ends_at',
    ];
    protected $fillable = [
        'slug',

        'name', 
        'first_name', 
        'last_name', 
        'email', 
        'password',  
        'avatar', 
        'role_id', 
 
        'activation_token', 

        'hide',
        'disabled',
    ];
    protected $casts = ['created_at' => 'date:d-m-Y'];
    // protected $touches = ['product_services', 'myproducts'];


    /**
     * Get all product-services from this model
     */
    public function product_services() 
    {
        return $this->morphMany('App\ProductService', 'productserviceable');
    }

    public function mysubscriptions()
    {
        return $this->hasMany('App\Subscription', 'user_id')->Subscribed();
    }



    public function getRouteKeyName()
    {
        return 'slug';
    }


    
    public function scopeisActivate($query)
    {
        return $query->where('activated', '=', 1);
    }

    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeWhereSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeWhereEmail($query, $email) 
    {
        return $query->where('email', '=', $email);
    }

    

    /**
     * Check if user has StripeCustomer ID exists.
     * Creates if not using checkout token
     * @param $token
     */
    public function createStripeCustomerIfNotExist($token=null)
    {
        if ($this->hasStripeId())
        {
            return true;
        }


        $this->createAsStripeCustomer($token);
    }

    // UNUSED
    public function activeSubscriptions()
    {
        $collection = $this->subscriptions;

        $filtered = $collection->filter(function($item, $key) {
            return $this->subscribed($item->name);
        });

        // $collection->each(function($item, $key){
        //     echo "<br>id: ".$item->id;
        //     echo "<br>name: ".$item->name;
        //     echo "<br>stripe_id: ".$item->stripe_id;
        //     echo "<br>stripe_plan: ".$item->stripe_plan;
        //     echo "<br>trial_ends_at: ".$item->trial_ends_at;
        //     echo "<br>ends_at: ".$item->ends_at;
        //     echo "<br>---------------------";
        // });
        // dd($collection);

        return $filtered;
    }

    //





    // public static function create(array $data)
    // {
    //     $data['slug'] = User::generateSlug();
    //     $data['name'] = $data['first_name'].' '.$data['last_name'];
    //     $data['activation_token'] = str_random(60);

    //     $model = static::query()->create($data);

    //     return $model;
    // }


    /**
     * Send the user a verification email.
     * 
     * @return void
     */
    // public function sendVerificationEmail()
    // {
    //     $this->notify(new VerifyEmail($this));
    // }
}
