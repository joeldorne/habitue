<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyMemberInvite extends Model
{
    protected $table = 'agencymemberinvites';
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'invited_by',
        'email',
        'agency_id',
        'token',
    ];

    public function agency() 
    {
        return $this->belongsTo('App\Agency');
    }

    public function scopeForAgency($query, $agency_id)
    {
        return $query->where('agency_id', '=', $agency_id);
    }

    public function scopeWhereEmail($query, $email)
    {
        return $query->where('email', '=', $email);
    }

    public function scopeWhereToken($query, $token)
    {
        return $query->where('token', '=', $token);
    }
}
