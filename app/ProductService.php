<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\ScopesForVisibilityAndEnability;

class ProductService extends Model
{
    use ScopesForVisibilityAndEnability;


    protected $table = 'product_services';
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'slug',
        'stripe_product_id',

        'name',
        'description',
        'image',

        // 'activate_form_url',

        'hide',
        'disabled',
    ];


    
    public function owner()
    {
        return $this->morphTo();
    }

    public function productserviceable()
    {
        return $this->morphTo();
    }



    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }
}
