<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'slug',

        'name',
        'description',
        'image',

        'currency',
        'payment_method',
        'pricing_type',

        'singular_price',
        'singular_subscription_period',
        'variation_prices',

        'activate_form_url',

        'hide',
        'disabled',
    ];
    // protected $casts = ['variation_prices'];
    // protected $with = ['productable'];



    public function seller()
    {
        return $this->morphTo();
    }
    public function productable()
    {
        return $this->morphTo();
    }



    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeSubscription($query)
    {
        return $query->where('payment_method', 'subscription');
    }
    public function scopeOneTimePay($query)
    {
        return $query->where('payment_method', 'one time pay');
    }

    public function scopeSingularPricing($query)
    {
        return $query->where('pricing_type', 'singular');
    }
    public function scopeVariationPricing($query)
    {
        return $query->where('pricing_type', 'variation');
    }
    
    public function scopeValid($query)
    {
        return $query->where('disabled', 0)->where('hide', 0);
    }

    public function scopeActive($query) 
    {
        return $query->where('disabled', '=', 0);
    }

    public function scopeDisabled($query) 
    {
        return $query->where('disabled', '=', 1);
    }

    public function scopeVisible($query) 
    {
        return $query->where('hide', '=', 0);
    }

    public function scopeHidden($query) 
    {
        return $query->where('hide', '=', 1);
    }



    // public static function create(array $data)
    // {
    //     $data['slug'] = Product::generateSlug();

    //     $data['user_id'] = \Auth::user()->id;

    //     $data['agency_id'] = \Auth::user()->agency[0]->id;

    //     $model = static::query()->create($data);

    //     return $model;
    // }

    // public static function generateSlug() 
    // {
    //     do {
    //         $slug = 'pr_'.str_random(40);
    //     } while(Product::WhereSlug($slug)->exists());

    //     return $slug;
    // }



    public function variation_prices_as_array()
    {
        return json_decode($this->variation_prices);
    }
    public function variation_prices_default_for_dropdown()
    {
        $json_prices = json_decode($this->variation_prices);

        foreach($json_prices as &$price) {

            if ($price->default == 'true') 
                return $price->name.' - $'.$price->amount;
        }

        return '';
    }
    public function variation_prices_select_options_droppable() 
    {
        $json_prices = json_decode($this->variation_prices);


        $options = '';
        $default = '';

        foreach($json_prices as &$price) {

            if ($price->default == 'true') 
                $default = $price->name.' - $'.$price->amount;

            $options .= '
                <div class="item" data-value="'.$price->name.' - '.$price->amount.'">'.$price->name.' - $'.$price->amount.'</div>
            ';
        }



        $dropdown = '';

        $dropdown .= '
            <div class="ui fluid selection dropdown">
                <input id="product_price_select" type="hidden" name="price" value="'.$default.'">
                <i class="dropdown icon"></i>
                <div class="default text">Price</div>
                <div class="menu">
        ';
        

        $dropdown .= $options;


        $dropdown .= '
                </div>
            </div>
        ';

        return $dropdown;
    }

    public function getPrice($price_name = null)
    {

        // if price_type is SINGULAR
        if ($this->price_type == 'singular') {
            
            return $this->price;

        }

        $json_prices = json_decode($this->price_variations);

        // if param price_name is null, return default from price variation
        if ($price_name == null) {

            foreach($json_prices as &$price) {

                if ($price->default == 'true')
                    return $price->amount;

            }

        }


        foreach($json_prices as &$price) {

            // if ($price->name == $price_name)
            //     return $price->amount;
            if ($price->amount == $price_name)
                return $price->amount;

        }

        
        return abort(404);
        
    }
}