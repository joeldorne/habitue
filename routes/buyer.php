<?php

/*
|--------------------------------------------------------------------------
| Buyer Routes
|--------------------------------------------------------------------------
|
|
*/



Route::get('/', 'BuyerController@index')->name('buyer');
Route::get('/dashboard', 'BuyerController@dashboard')->name('buyer.dashboard');



Route::get('/billings', 'BillingController@index')->name('buyer.billings');
Route::get('/billings/cards', 'BillingCardController@cards')->name('buyer.billings.cards');
Route::get('/billings/cards/add', 'BillingCardController@addCard')->name('buyer.billings.cards.add');
Route::post('/billings/cards/store', 'BillingCardController@storeCard')->name('buyer.billings.cards.store');
Route::get('/billings/cards/{card}/makedefault', 'BillingCardController@makeDefault')->name('buyer.billings.cards.makedefault');
Route::get('/billings/cards/{card}/remove', 'BillingCardController@remove')->name('buyer.billings.cards.remove');



Route::get('/shop', 'ShopController@index')->name('buyer.shop');

Route::get('/shop/products', 'ShopController@products')->name('buyer.shop.products');
Route::get('/shop/purchasing/{product}', 'ShopController@purchasing')->name('buyer.shop.purchasing');
Route::post('/shop/purchased', 'ShopController@purchased')->name('buyer.shop.purchased');

Route::get('/shop/subscriptions', 'ShopController@subscriptions')->name('buyer.shop.subscriptions');
Route::get('/shop/subscribing/{product_service}', 'ShopController@subscribing')->name('buyer.shop.subscription');

Route::get('/subscription/checkout/{product_service}/plan/{stripe_plan}', 'SubscriptionPaymentController@checkoutForm')->name('buyer.subscription.checkoutform');
Route::post('/subscription/checkout', 'SubscriptionPaymentController@checkout')->name('buyer.subscription.checkout');


Route::get('/mysubscription', 'MySubscriptionController@index')->name('buyer.mysubscription');
Route::get('/mysubscription/{subscription}/cancel', 'MySubscriptionController@cancel')->name('buyer.mysubscription.cancel');
Route::get('/mysubscription/{subscripiton}/downloadInvoice', 'MySubscriptionController@downloadInvoice')->name('buyer.mysubscription.downloadInvoice');



Route::get('/purchases', 'PurchaseController@purchases')->name('buyer.purchases');

// Route::get('/payment-reports', 'PaymentReportController@index')->name('voyager.payment-reports.index');


Route::get('/stripetest/test1', 'StripeTestController@test1');


/**
 * CARTALYST TESTS
 */
Route::get('/cartalyst/test/customer', 'CartalystTestController@customer');
Route::get('/cartalyst/test/subscriptions', 'CartalystTestController@subscriptions');
Route::get('/cartalyst/test/subscribe', 'CartalystTestController@subscribe');


/**
 * Stripe Test
 */
Route::get('/stripe/test/customer', 'StripeTestController@customer');
Route::get('/stripe/test/subscriptions', 'StripeTestController@subscriptions');
Route::get('/stripe/test/subscriptions/cancel', 'StripeTestController@cancel_subscriptions');
Route::get('/stripe/test/subscribe', 'StripeTestController@subscribe');
Route::get('/stripe/test/subscription/get', 'StripeTestController@getSubscription');
