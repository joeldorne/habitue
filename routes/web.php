<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');


/* ***************** */
/* ***************** */
/* Auth************* */

Auth::routes();
// Route::get('/register/invited/{token}/{role}', 'RegisterController@invitedForm')->name('register.invited');
// Route::post('/register/invited', 'RegisterController@invitedStore')->name('register.invite.store');


/* ***************** */
/* ***************** */
/* Guest************ */

Route::get('/guest', 'HomeController@guest')->name('guest');


/* ***************** */
/* ***************** */
/* User Profile***** */

Route::get('/u/profile', 'UserController@profile')->name('user.profile');
Route::get('/u/profile/change', 'UserController@edit')->name('user.profile.edit');
Route::get('/u/profile/passwordchange', 'UserController@changepassword')->name('user.profile.password.edit');

Route::post('/u/profile/change', 'UserController@update')->name('user.profile.update');
Route::post('/u/profile/passwordchange', 'UserController@updatepassword')->name('user.profile.password.update');


/* ***************** */
/* ***************** */
/* Billings********* */

Route::get('/billings', 'BillingController@index')->name('billings');
Route::get('/billings/cards', 'BillingController@cards')->name('billings.cards');


/* ***************** */
/* ***************** */
/* ***************** */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


/* ***************** */
/* ***************** */
/* Stripe Webhook*** */

Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);

/* ***************** */
/* ***************** */
/* ***************** */


/* ***************** */
/* ***************** */
/* ***************** */


/* ***************** */
/* ***************** */
/* ***************** */


/* ***************** */
/* ***************** */
/* ***************** */


/* ***************** */
/* ***************** */
/* ***************** */


/* ***************** */
/* ***************** */
/* ***************** */




Route::get('/stripetests/form1', function(){
    return view('stripetests/forms/card-element-1');
});
Route::get('/stripetests/form2', function(){
    return view('stripetests/forms/card-element-2');
});

Route::get('/admin/stripetest/product/create', 'Admin\StripeTestController@create');