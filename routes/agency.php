<?php

/*
|--------------------------------------------------------------------------
| Agency Routes
|--------------------------------------------------------------------------
|
|
*/



Route::get('/create', 'AgencyController@create')->name('agency.start');
Route::post('/create', 'AgencyController@store')->name('agency.store');


Route::get('/', 'AgencyController@dashboard')->name('agency');


/*
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/


Route::group([
  // 'domain' => config('session.domain_url'),
  // 'middleware' => ['agencysubdomain']
], function () { 

  

  Route::get('/dashboard', 'AgencyController@dashboard')->name('agency.dashboard');



  Route::get('/profile', 'AgencyController@profile')->name('agency.profile');
  Route::get('/profile/change', 'AgencyController@editprofile')->name('agency.profile.edit');

  

  Route::get('/team', 'AgencyTeamController@team')->name('agency.team');

  Route::get('/team/invites', 'AgencyTeamController@teamInvites')->name('agency.team.invites');
  Route::get('/team/invites/resendemail/{email}', 'AgencyTeamController@teamInviteResendEmail')->name('agency.team.invites.resendemail');
  Route::get('/team/invites/remove/{email}', 'AgencyTeamController@teamInviteRemove')->name('agency.team.invites.remove');

  Route::get('/team/{agency_member}', 'AgencyTeamController@member')->name('agency.team.member');



  // Route::get('/clients', 'AgencyClientController@clients')->name('agency.clients');
  // Route::get('/clients/invite', 'AgencyClientController@invite')->name('agency.clients.invite');
  // Route::post('/clients/invite', 'AgencyClientController@storeInvite')->name('agency.clients.invite.store');



  Route::get('/chats', 'AgencyDiscussionController@index')->name('agency.chats');



  Route::get('/tasks', 'AgencyTaskController@index')->name('agency.tasks');



  Route::get('/billings', 'BillingController@index')->name('agency.billings');

  Route::get('/billings/cards', 'BillingCardController@cards')->name('agency.billings.cards');
  Route::get('/billings/cards/add', 'BillingCardController@addCard')->name('agency.billings.cards.add');
  Route::get('/billings/cards/{card}/makedefault', 'BillingCardController@makeDefault')->name('agency.billings.cards.makedefault');
  Route::get('/billings/cards/{card}/remove', 'BillingCardController@remove')->name('agency.billings.cards.remove');



  Route::get('/products', 'ProductController@products')->name('agency.products');
  Route::get('/products/create', 'ProductController@create')->name('agency.products.create');
  Route::get('/products/edit/{agency_product}', 'ProductController@edit')->name('agency.products.edit');
  Route::get('/products/settoactive/{agency_product}', 'ProductController@setToActive')->name('agency.products.settoactive');
  Route::get('/products/settoinactive/{agency_product}', 'ProductController@setToInactive')->name('agency.products.settoinactive');
  // Route::post('/products/temp_image/store', 'ProductController@tempImageStore');

  Route::get('/product/{slug}', 'ProductController@show')->name('agency.products.show');

  // Route::get('/cropper', 'ProductController@cropper')->name('agency.cropper');
  // Route::post('/cropper', 'ProductController@postcropper');



  Route::get('/paymentreports', 'PaymentReportController@index')->name('agency.paymentreport');
  Route::get('/paymentreports/purchases', 'PaymentReportController@purchases')->name('agency.paymentreport.purchases');



});


/*
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/




Route::post('/profile/change', 'AgencyController@updateprofile')->name('agency.profile.update');



Route::post('/team/invites', 'AgencyTeamController@teamInvitesStore')->name('agency.team.invites.store');
Route::post('/team/changeposition', 'AgencyTeamController@updatePosition')->name('agency.team.invites.changeposition');



Route::post('/products/store', 'ProductController@store')->name('agency.products.store');
Route::post('/products/update', 'ProductController@update')->name('agency.products.update');
Route::post('/products/store/image', 'ProductController@storeImage')->name('agency.products.store.image');



Route::post('/billings/cards/store', 'BillingCardController@storeCard')->name('agency.billings.cards.store');



Route::post('/paymentreports/purchases/updatecompletion', 'PaymentReportController@updateCompletion')->name('agency.paymentreport.updatecompletion');
Route::post('/paymentreports/purchases/updatestatus', 'PaymentReportController@updateStatus')->name('agency.paymentreport.updatestatus');



// Route::group([
  // 'domain' => config('session.domain_url')
// ], function () { 


  // Route::get


// });