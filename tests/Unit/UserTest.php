<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\DatabaseTransactions;
// use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\User;

use App\Repositories\User\UserRepository;


class UserTest extends TestCase
{
    use WithFaker;

    private $user_data;


    public function setUp()
    {
        parent::setUp();

        $this->user_data = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->safeEmail(),
            'password' => $this->faker->password(),
            'role_id' => $this->faker->randomElement($array = array (1,2,3)),
            'slug' => 'us_'.str_random(10)
        ];
    }


    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }


    /** @test */
    public function it_returns_null_when_deleting_a_non_existing_user()
    {
        $userRepo = new UserRepository(new User);
        $delete = $userRepo->delete();

        $this->assertNull($delete);
    }


    /** @test */
    public function it_should_throw_update_error_exception_when_the_user_has_failed_to_update()
    {
        $this->expectException(\App\Repositories\User\Exceptions\UpdateUserErrorException::class);

        $new_user = factory(User::class)->create();
        $userRepo = new UserRepository($new_user);

        $data = ['slug' => null];
        $userRepo->update($data);
    }


    /** @test */
    public function it_should_throw_not_found_error_exception_when_the_user_is_not_found()
    {
        $this->expectException(\App\Repositories\User\Exceptions\UserNotFoundException::class);

        $userRepo = new UserRepository(new User);
        $userRepo->find(9999);
    }


    /** @test */
    public function it_should_throw_an_error_when_the_required_columns_are_not_filled()
    {
        $this->expectException(\App\Repositories\User\Exceptions\CreateUserErrorException::class);

        $userRepo = new UserRepository(new User);
        $userRepo->create([]);
    }


    /** @test */
    public function it_can_delete_the_user()
    {
        $new_user = factory(User::class)->create();

        $userRepo = new UserRepository($new_user);
        $delete = $userRepo->delete();

        $this->assertTrue($delete);
    }


    /** @test */
    public function it_can_update_the_user()
    {
        $new_user = factory(User::class)->create();

        $userRepo = new UserRepository($new_user);
        $update = $userRepo->update($this->user_data);

        $this->assertTrue($update);
        $this->assertEquals($this->user_data['first_name'], $new_user->first_name);
        $this->assertEquals($this->user_data['last_name'], $new_user->last_name);
        $this->assertEquals($this->user_data['email'], $new_user->email);
        $this->assertNotEmpty($new_user->password);
        $this->assertEquals($this->user_data['role_id'], $new_user->role_id);

        $userRepo->delete();
    }


    /** @test */
    public function it_can_find_the_user()
    {
        $new_user = factory(User::class)->create();
        $userRepo = new UserRepository(new User);
        $found = $userRepo->find($new_user->id);

        $this->assertInstanceOf(User::class, $found);
        $this->assertEquals($found->first_name, $new_user->first_name);
        $this->assertEquals($found->last_name, $new_user->last_name);
        $this->assertEquals($found->email, $new_user->email);
        $this->assertNotEmpty($found->password);
        $this->assertEquals($found->role_id, $new_user->role_id);

        $new_user->delete();
    }


    /** @test */
    public function it_can_create_a_user()
    {
        $userRepo = new UserRepository(new User);
        $new_user = $userRepo->create($this->user_data);

        $this->assertInstanceOf(User::class, $new_user);
        $this->assertEquals($this->user_data['first_name'], $new_user->first_name);
        $this->assertEquals($this->user_data['last_name'], $new_user->last_name);
        $this->assertEquals($this->user_data['email'], $new_user->email);
        $this->assertNotEmpty($new_user->password);
        $this->assertEquals($this->user_data['role_id'], $new_user->role_id);

        $userRepo->delete();
    }

    /** @test */
    public function find_user_super_admin_is_in_user_database()
    {
        $this->assertTrue(true);
        // $user = new UserRepository(Auth::user());

        // $this->seeInDatabase('users', [
            // 'id' => 1,
            // 'role_id' => 1,
        //     'email' => 'admin@habitue.com',
        // ]);
    }


    /** @test */
    public function can_make_an_admin_user()
    {
        $data = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john.doe@email.com',
            'password' => 'qwqwqw',
            'role_id' => 1,
            'slug' => 'us_qwertyuiop'
        ];

        $admin_user = User::make($data);

        $this->assertInstanceOf(User::class, $admin_user);
        $this->assertEquals($data['first_name'], $admin_user->first_name);
        $this->assertEquals($data['last_name'], $admin_user->last_name);
        $this->assertEquals($data['email'], $admin_user->email);
        $this->assertNotEmpty($admin_user->password);
        $this->assertEquals($data['role_id'], $admin_user->role_id);
        $this->assertEquals($data['slug'], $admin_user->slug);
    }
}
