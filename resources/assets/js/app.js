
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
var vueResource = require('vue-resource')
Vue.use(vueResource)

// Vue.prototype.$http = axios;


// Add textarea autosize to be used inside vue
import VueTextareaAutosize from 'vue-textarea-autosize'
Vue.use(VueTextareaAutosize)


// get csrf token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#meta_token').getAttribute('content');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// Vue.component('product-edit-form-with-cropper', require('./components/ProductEditFormWithCropperComponent.vue'));
Vue.component('product-crud-container', require('./components/product-crud/0-Container.vue'));
Vue.component('product-crud-basic', require('./components/product-crud/1-Basic.vue'));
Vue.component('product-crud-image', require('./components/product-crud/2-Image.vue'));
Vue.component('product-crud-pricing', require('./components/product-crud/4-Pricing.vue'));
Vue.component('product-crud-misc', require('./components/product-crud/5-Misc.vue'));
Vue.component('product-crud-visibility', require('./components/product-crud/6-Visibility.vue'));


Vue.component('subscription-crud-container', require('./components/subscription-crud/0-Container.vue'));
Vue.component('subscription-crud-basic', require('./components/subscription-crud/1-Basic.vue'));
Vue.component('subscription-crud-image', require('./components/subscription-crud/2-Image.vue'));
Vue.component('subscription-crud-pricing', require('./components/subscription-crud/3-Pricing.vue'));
Vue.component('subscription-crud-plan', require('./components/subscription-crud/includes/BillingPlan.vue'));
Vue.component('subscription-crud-visibility', require('./components/subscription-crud/4-Visibility.vue'));


Vue.component('agency-member-list', require('./components/AgencyMemberList.vue'));
Vue.component('agency-member', require('./components/AgencyMember.vue'));


Vue.component('products-sold-list', require('./components/payment-reports/ProductsSoldList.vue'));
Vue.component('products-sold', require('./components/payment-reports/ProductsSold.vue'));


Vue.component('products-purchases-list', require('./components/ProductsPurchasesList.vue'));
Vue.component('products-purchases', require('./components/ProductsPurchases.vue'));



Vue.directive('focus', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        // Focus the element
        el.focus()
    }
})



const app = new Vue({
    el: '#app',
    data: {
        var1: 10,
    },

    methods: {
        method1: function()
        {
        },

        method2: function()
        {
            //
        },
    },
});



window.Event = new class {
    constructor() 
    {
        this.vue = new Vue();
    }

    fire(event, data = null) 
    {
        this.vue.$emit(event, data);
    }

    listen() 
    {
        this.vue.$on(event, callback);
    }
}
