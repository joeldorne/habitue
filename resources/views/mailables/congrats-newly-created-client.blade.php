@component('mail::message')
# Introduction

Congratulations, you just made you first step to start purchasing.

@component('mail::button', ['url' => route('products.list'), 'color' => 'green'])
Click here to start browsing products
@endcomponent

@component('mail::panel')
This is the panel content.
@endcomponent

Thanks,<br>
{{ config('mail.from.name') }}
@endcomponent
