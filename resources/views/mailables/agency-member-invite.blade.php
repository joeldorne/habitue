@extends('layouts.mailables')


@section('title')
Habitue Invite - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main container segment basic">
    <p>Email: {{ $email }}</p>
    <p>Token: {{ $token }}</p>
    <a href="{{ route('register.invited', ['token'=>$token, 'role'=>'agency']) }}" class="ui green big button">Signup and Join</a>
  </div>
@endsection


@section('script-footer')
@endsection