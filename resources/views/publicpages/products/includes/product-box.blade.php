<div class="ui card">


  <div class="image">  
    <img src="/{{ $product->image }}">
  </div>


  <div class="content">
    
    
    <a class="header">{{ $product->name }}</a>


    <div class="description">
      {{ $product->description }}
    </div>


  </div>


  <div class="extra content">


    @if ( $product->price_type == 'singular' )

      <span class="">
        $ {{ $product->price }}
      </span>

    @else

      <span class="">
        {!! $product->selectPrices() !!}
      </span>

    @endif

  </div>



  <div class="extra content">

    @if ( $product->subscription == 1 )

      <a class="ui right floated teal button">
        <i class="money bill alternate outline icon"></i>
        Subscribe
      </a>

    @else

      <a href="/p/purchasing/{{ $product->slug }}" class="ui right floated primary button">
        <i class="money bill alternate outline icon"></i>
        Purchase
      </a>

    @endif


  </div>
</div>