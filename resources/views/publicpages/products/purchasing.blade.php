@extends('layouts.public')


@section('title')
Purchasing
@endsection


@section('script-header')
@endsection


@section('content')

  <div class="ui grid">
  

    <div class="four wide column">


      <div class="ui main container segment basic">
        <div class="ui centered card">


            <div class="content">
              <div class="right floated meta hide">14h</div>
              <img class="ui avatar image" src="/{{ $product->image }}"> {{ $product->agency->name }}
            </div>


            <div class="image hide">
              <img>
            </div>


            <div class="content">
              <a class="header">{{ $product->name }}</a>
              <div class="meta">
                <span class="date">Created: {{ $product->updated_at->format('M d, Y') }}</span>
              </div>
              <div class="description">
                {{ $product->description }}
              </div>
            </div>


            <div class="extra content hide">
              <span class="right floated">
                Joined in 2013
              </span>
              <span>
                <i class="user icon"></i>
                75 Friends
              </span>
            </div>


            <div class="content">
              <input id="product_price_type" type="hidden" name="product_price_type" value="{{ $product->price_type }}">

              @if ( $product->price_type == 'singular' )

                <input id="product_price" type="hidden" name="price" value="{{ $product->price }}">

                <span class="">

                  $ {{ $product->price }}

                </span>

              @else

                {!! $product->selectPrices() !!}

              @endif
              
            </div>


        </div>
      </div>


    </div>






    <div class="twelve wide column">


      <div class="ui main segment basic">
        
        @if ($customer->sources->total_count > 0)

          <div class="ui very relaxed list">

            @foreach ($customer->sources->data as $card)

              
              <div class="item">
                <img class="ui avatar image" src="/images/semantics/wireframe/image.png">
                <div class="content">

                  <a class="header">#### #### #### {{ $card->last4 }}</a>

                  <div class="description">
                    {{ $card->brand }} 

                    @if ($customer->default_source == $card->id)
                      <span class="ui label basic">Default</span>
                    @endif
                  </div>


                  <div class="description">

                    @if ($customer->default_source == $card->id)
                      <form method="post" action="/p/purchased">
                        {{ csrf_field() }}
                        <input type="hidden" name="card" value="{{ $card->id }}">
                        <input type="hidden" name="product" value="{{ $product->slug }}">
                        <input type="hidden" name="price_name" value="">
                        <button class="ui button green">Use this Card and Pay for $<span class="price_text">$100</span></button>
                      </form>
                    @else
                      <form method="post" action="/p/purchased">
                        {{ csrf_field() }}
                        <input type="hidden" name="card" value="{{ $card->id }}">
                        <input type="hidden" name="product" value="{{ $product->slug }}">
                        <input type="hidden" name="price_name" value="">
                        <button class="ui button">Use this Card and Pay for $<span class="price_text">$100</span></button>
                      </form>
                    @endif

                  </div>


                </div>
              </div>

            @endforeach

          </div>

        @else

          <div class="ui message">
            <div class="header">
              You haven't added a billing card yet
            </div>
            <p>To add a billing card please click <a href="{{ route('billings.cards') }}">here</a>.</p>
          </div>

        @endif



      </div>

    </div>

  </div>
@endsection


@section('script-footer')
<script>
  var $product_price_select = $('#product_price_select');
  var $input_price_name = $('input[name="price_name"]');
  var $price_text = $('.price_text');

  var $product_price_type = $('#product_price_type');
  var $product_price = $('#product_price');

  $product_price_select.change(function(){
    price = get_price_value($product_price_select.val());
    $input_price_name.val(price);

    $price_text.text(price);

  });

  if ($product_price_type.val() == 'variation'){console.log('variation', )
    // set price name on load if price type is variation
    var price = get_price_value($product_price_select.val());
    
    $input_price_name.val(price);
    $price_text.text(price);
  }
  else {
    // set price name on load if price type is singular
    $input_price_name.val($product_price.val());
    $price_text.text($product_price.val());
  }

  function get_price_value(value)
  {
    return value.split(' - ')[1];
  }

</script>
@endsection