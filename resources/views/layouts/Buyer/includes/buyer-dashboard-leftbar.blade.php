<div class="ui visible inverted left vertical sidebar menu very thin icon">
  <a class="item" href="/a/dashboard">
    <i class="grid layout icon"></i>
  </a>
  <a class="item" id="side-menu-purchases" href="{{route('buyer.purchases')}}"><i class="dollar sign icon"></i></a>
  <a class="item" id="side-menu-stores" href="{{route('buyer.shop')}}"><i class="shoppng cart icon"></i></a>
</div>