<!DOCTYPE html>
<html lang="en" class="">
  <head>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="{{ config('app.name') }}">
    <meta id="meta_token" name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{URL::asset('css/app.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('vendor/semantic-ui/semantic.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('css/global.css')}}" rel="stylesheet" type="text/css" />

    @yield('script-header')
  </head>

  <body>
    <div id="app">
      @include('layouts.includes.loader')

      @include('layouts.includes.agency-home-topbar')
      
      <div class="mypusher">
        @yield('content')
      </div>
    </div>

    <script src="{{URL::asset('js/app.js')}}"></script>
    <script src="{{URL::asset('vendor/semantic-ui/semantic.min.js')}}"></script>

    <script src="{{URL::asset('js/layouts/agency-home.js')}}"></script>

    @yield('script-footer')
  </body>
</html>