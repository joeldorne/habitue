<div class="ui visible inverted left vertical sidebar menu very thin icon">
  <a class="item" href="/a/dashboard">
    <i class="grid layout icon"></i>
  </a>
  <a class="item" id="side-menu-users" href="/admin/users"><i class="users icon"></i></a>
  <a class="item" id="side-menu-products" href="/admin/products"><i class="tv icon"></i></a>
  <a class="item" id="side-menu-subscriptions" href="/admin/subscriptions"><i class="tv icon"></i></a>
  <a class="item" id="side-menu-paymentreports" href="{{route('voyager.payment-reports.sales')}}"><i class="dollar sign icon"></i></a>
  <a class="item" id="side-menu-settings" href="/admin/settings"><i class="cog icon"></i></a>
</div>