<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="">
  <head>
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="{{ config('app.name') }}">
    <meta id="meta_token" name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{URL::asset('css/app.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('vendor/semantic-ui/semantic.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('css/global.css')}}" rel="stylesheet" type="text/css" />

    @yield('script-header')
  </head>

  <body>

    @if ( Auth::user()->hasRole('agency') )
        @if ( Auth::user()->agency->count() )
            <div id="app">
                @include('layouts.includes.loader')

                @include('layouts.Agency.includes.agency-dashboard-leftbar')
                @include('layouts.Agency.includes.agency-dashboard-rightbar')

                <div class="mypusher m-l-60">
                    @include('layouts.Agency.includes.agency-dashboard-topbar')

                    @yield('content')
                </div>
            </div>
        @else
            <div id="app">
                @include('layouts.includes.loader')

                @include('layouts.Agency.includes.agency-home-topbar')
                
                <div class="mypusher">
                    @yield('content')
                </div>
            </div>
        @endif
    @endif


    @if ( Auth::user()->hasRole('client') )
        @if ( Auth::user()->client->count() )
            <div id="app">
                @include('layouts.includes.loader')

                @include('layouts.Client.includes.client-dashboard-leftbar')
                @include('layouts.Client.includes.client-dashboard-rightbar')

                <div class="mypusher m-l-60">
                    @include('layouts.Client.includes.client-dashboard-topbar')

                    @yield('content')
                </div>
            </div>
        @else
            <div id="app">
                @include('layouts.includes.loader')

                @include('layouts.Client.includes.client-home-topbar')
                
                <div class="mypusher">
                    @yield('content')
                </div>
            </div>
        @endif
    @endif


    @if ( Auth::user()->hasRole('buyer') )
        <div id="app">
            @include('layouts.includes.loader')

            @include('layouts.Buyer.includes.buyer-dashboard-leftbar')
            @include('layouts.Buyer.includes.buyer-dashboard-rightbar')

            <div class="mypusher m-l-60">
                @include('layouts.Buyer.includes.buyer-dashboard-topbar')
                @yield('content')
            </div>
        </div>
    @endif


    @if ( Auth::user()->hasRole('admin') )
        <div id="app">
            @include('layouts.includes.loader')

            @include('layouts.Admin.includes.dashboard-leftbar')
            @include('layouts.Admin.includes.dashboard-rightbar')

            <div class="mypusher m-l-60">
                @include('layouts.Admin.includes.dashboard-topbar')
                @yield('content')
            </div>
        </div>
    @endif



    <script src="{{URL::asset('js/app.js')}}"></script>
    <script src="{{URL::asset('vendor/semantic-ui/semantic.min.js')}}"></script>



    @if ( Auth::user()->hasRole('admin') )
        <script src="{{URL::asset('js/layouts/admin-dashboard.js')}}"></script>
    @endif

    @if ( Auth::user()->hasRole('buyer') )
        <script src="{{URL::asset('js/layouts/buyer-dashboard.js')}}"></script>
    @endif

    @if ( Auth::user()->hasRole('agency') )
        @if ( Auth::user()->agency->count() )
            <script src="{{URL::asset('js/layouts/agency-dashboard.js')}}"></script>
        @else
            <script src="{{URL::asset('js/layouts/agency-home.js')}}"></script>
        @endif
    @endif

    @if ( Auth::user()->hasRole('client') )
        @if ( Auth::user()->client->count() )
            <script src="{{URL::asset('js/layouts/client-dashboard.js')}}"></script>
        @else
            <script src="{{URL::asset('js/layouts/client-home.js')}}"></script>
        @endif
    @endif
    

    @yield('script-footer')
  </body>
</html>