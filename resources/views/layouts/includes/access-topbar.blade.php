<div class="ui large menu attached">
  <div class="container">
    <div class="header item">
      <a href="/">{{ config('app.name') }}</a>
    </div>

    <div class="right menu">
      <a class="item" href="/login">
        Login
      </a>
      <a class="item" href="/register">
        Register
      </a>
    </div>
  </div>
</div>