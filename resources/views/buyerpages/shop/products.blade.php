@extends('layouts.auth_all')


@section('title')
Products
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">

        <h3>Shop Products</h3>

        @include('buyerpages.shop.partials.cardlist-product', $products)

    </div>
@endsection


@section('script-footer')
@endsection