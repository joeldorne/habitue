<!-- Stripe Form -->
<div class="cell example example2">
    <form id="form-stripe" method="post" action="{{ route('buyer.shop.purchased') }}">
        {{ csrf_field() }}

        <input type="hidden" name="product_slug" value="{{ $product->slug }}">
        <input type="hidden" name="price_name">
        <input type="hidden" name="price_value">

        <div data-locale-reversible="">
            <div class="row">
                <div class="field">
                    <input id="example2-address" data-tid="elements_examples.form.address_placeholder" class="input empty" type="text" name="address" placeholder="185 Berry St">
                    <label for="example2-address" data-tid="elements_examples.form.address_label">Address</label>
                    <div class="baseline"></div>
                </div>
            </div>

            <div class="row" data-locale-reversible="">
                <div class="field half-width">
                    <input id="example2-city" data-tid="elements_examples.form.city_placeholder" class="input empty" type="text" name="city" placeholder="San Francisco" >
                    <label for="example2-city" data-tid="elements_examples.form.city_label">City</label>
                    <div class="baseline"></div>
                </div>

                <div class="field quarter-width">
                    <input id="example2-state" data-tid="elements_examples.form.state_placeholder" class="input empty" type="text" name="state" placeholder="CA" >
                    <label for="example2-state" data-tid="elements_examples.form.state_label">State</label>
                    <div class="baseline"></div>
                </div>

                <div class="field quarter-width">
                    <input id="example2-zip" data-tid="elements_examples.form.postal_code_placeholder" class="input empty" type="text" name="zip" placeholder="94107" >
                    <label for="example2-zip" data-tid="elements_examples.form.postal_code_label">ZIP</label>
                    <div class="baseline"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="field">
                <div id="example2-card-number" class="input empty StripeElement"><div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;"><iframe frameborder="0" allowtransparency="true" scrolling="no" name="__privateStripeFrame6" allowpaymentrequest="true" src="https://js.stripe.com/v3/elements-inner-card-c20b2c01a31142f7c49ef9874cd24af2.html#style[base][color]=%2332325D&amp;style[base][fontWeight]=500&amp;style[base][fontFamily]=Source+Code+Pro%2C+Consolas%2C+Menlo%2C+monospace&amp;style[base][fontSize]=16px&amp;style[base][fontSmoothing]=antialiased&amp;style[base][::placeholder][color]=%23CFD7DF&amp;style[base][:-webkit-autofill][color]=%23e39f48&amp;style[invalid][color]=%23E25950&amp;style[invalid][::placeholder][color]=%23FFCCA5&amp;locale=en&amp;componentName=cardNumber&amp;wait=true&amp;rtl=false&amp;features[noop]=false&amp;origin=https%3A%2F%2Fstripe.github.io&amp;referrer=https%3A%2F%2Fstripe.github.io%2Felements-examples%2F&amp;controllerId=__privateStripeController0" title="Secure payment input frame" style="border: none !important; margin: 0px !important; padding: 0px !important; width: 1px !important; min-width: 100% !important; overflow: hidden !important; display: block !important; height: 19.2px;"></iframe><input class="__PrivateStripeElement-input" aria-hidden="true" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"><input class="__PrivateStripeElement-safariInput" aria-hidden="true" tabindex="-1" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"></div></div>
                <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">Card number <span style="color:red">*</span></label>
                <div class="baseline"></div>
            </div>
        </div>

        <div class="row">
            <div class="field half-width">
                <div id="example2-card-expiry" class="input empty StripeElement"><div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;"><iframe frameborder="0" allowtransparency="true" scrolling="no" name="__privateStripeFrame7" allowpaymentrequest="true" src="https://js.stripe.com/v3/elements-inner-card-c20b2c01a31142f7c49ef9874cd24af2.html#style[base][color]=%2332325D&amp;style[base][fontWeight]=500&amp;style[base][fontFamily]=Source+Code+Pro%2C+Consolas%2C+Menlo%2C+monospace&amp;style[base][fontSize]=16px&amp;style[base][fontSmoothing]=antialiased&amp;style[base][::placeholder][color]=%23CFD7DF&amp;style[base][:-webkit-autofill][color]=%23e39f48&amp;style[invalid][color]=%23E25950&amp;style[invalid][::placeholder][color]=%23FFCCA5&amp;locale=en&amp;componentName=cardExpiry&amp;wait=true&amp;rtl=false&amp;features[noop]=false&amp;origin=https%3A%2F%2Fstripe.github.io&amp;referrer=https%3A%2F%2Fstripe.github.io%2Felements-examples%2F&amp;controllerId=__privateStripeController0" title="Secure payment input frame" style="border: none !important; margin: 0px !important; padding: 0px !important; width: 1px !important; min-width: 100% !important; overflow: hidden !important; display: block !important; height: 19.2px;"></iframe><input class="__PrivateStripeElement-input" aria-hidden="true" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"><input class="__PrivateStripeElement-safariInput" aria-hidden="true" tabindex="-1" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"></div></div>
                <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiration <span style="color:red">*</span></label>
                <div class="baseline"></div>
            </div>

            <div class="field half-width">
                <div id="example2-card-cvc" class="input empty StripeElement"><div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;"><iframe frameborder="0" allowtransparency="true" scrolling="no" name="__privateStripeFrame8" allowpaymentrequest="true" src="https://js.stripe.com/v3/elements-inner-card-c20b2c01a31142f7c49ef9874cd24af2.html#style[base][color]=%2332325D&amp;style[base][fontWeight]=500&amp;style[base][fontFamily]=Source+Code+Pro%2C+Consolas%2C+Menlo%2C+monospace&amp;style[base][fontSize]=16px&amp;style[base][fontSmoothing]=antialiased&amp;style[base][::placeholder][color]=%23CFD7DF&amp;style[base][:-webkit-autofill][color]=%23e39f48&amp;style[invalid][color]=%23E25950&amp;style[invalid][::placeholder][color]=%23FFCCA5&amp;locale=en&amp;componentName=cardCvc&amp;wait=true&amp;rtl=false&amp;features[noop]=false&amp;origin=https%3A%2F%2Fstripe.github.io&amp;referrer=https%3A%2F%2Fstripe.github.io%2Felements-examples%2F&amp;controllerId=__privateStripeController0" title="Secure payment input frame" style="border: none !important; margin: 0px !important; padding: 0px !important; width: 1px !important; min-width: 100% !important; overflow: hidden !important; display: block !important; height: 19.2px;"></iframe><input class="__PrivateStripeElement-input" aria-hidden="true" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"><input class="__PrivateStripeElement-safariInput" aria-hidden="true" tabindex="-1" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;"></div></div>
                <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">CVC <span style="color:red">*</span></label>
                <div class="baseline"></div>
            </div>
        </div>

        <button type="submit" id="btn_add" data-tid="elements_examples.form.pay_button" class="ui green button">Pay $<span class="text_price"></span></button>

        <div class="error" role="alert">
            <p>&nbsp;</p>
            <span class="message" style=""></span>
        </div>
    </form>
</div>