@extends('layouts.auth_all')


@section('title')
Purchasing
@endsection


@section('script-header')
<script src="https://js.stripe.com/v3/"></script>

<script src="{{URL::asset('vendor/stripe-elements-example/index.js')}}" data-rel-js></script>

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">

<link href="{{URL::asset('vendor/stripe-elements-example/base.css')}}" rel="stylesheet" type="text/css" data-rel-css="" />
<link href="{{URL::asset('vendor/stripe-elements-example/example2/style.css')}}" rel="stylesheet" type="text/css" data-rel-css="" />
@endsection


@section('content')

<div class="ui grid">
  

    <!-- Product Info -->
    <div class="four wide column">
        <div class="ui main container segment basic">

            <div class="ui centered card">
                <div class="content">
                    <img class="ui avatar image" src="/{{ $product->image }}">
                </div>


                <div class="content">
                    <a class="header">{{ $product->name }}</a>
                    <div class="meta">
                        <span class="date">Created: {{ $product->updated_at->format('M d, Y') }}</span>
                    </div>
                    <div class="description">
                        {{ $product->description }}
                    </div>
                </div>


                <div class="content">
                    <input id="product_price_type" type="hidden" name="product_price_type" value="{{ $product->pricing_type }}">

                    @if ( $product->pricing_type == 'singular' )

                        <input id="product_price" type="hidden" name="price" value="{{ $product->singular_price }}">

                        <span class="">
                            Amount: $ {{ $product->singular_price }}
                        </span>

                    @else

                        @include('buyerpages.shop.partials.dropdown-variation-prices', 
                        [
                            'array_prices' => $product->variation_prices_as_array(), 
                            'default_price' => $product->variation_prices_default_for_dropdown()
                        ])

                    @endif
                </div>
            </div>

        </div>
    </div>
    <!-- End Product Info -->


    <!-- Stripe Checkout Form -->    
    <div class="eight wide column">
        
        <div class="ui main container segment basic">

            @include('buyerpages.shop.partials.stripe-example-form', ['product' => $product])

        </div>

    </div>
    <!-- End Stripe Checkout Form -->


</div>
@endsection


@section('script-footer')
<script>
    var stripe = Stripe('{{ config("services.stripe.key") }}');
</script>


<script src="{{URL::asset('vendor/stripe-elements-example/l10n.js')}}" data-rel-js></script>

<script src="{{URL::asset('vendor/stripe-elements-example/example2/script.js')}}" data-rel-js></script>


<script>
    var $select_product_price = $('#product_price_select');
    var $input_price_name = $('input[name="price_name"]');
    var $input_price_value = $('input[name="price_value"]');
    var $text_price = $('.text_price');

    var $product_price_type = $('#product_price_type');
    var $product_price = $('#product_price');

    var price_name, price;



    if ($product_price_type.val() == 'variation'){

        // console.log('variation')
        $select_product_price.change(function(){

            price_name = get_price_name($select_product_price.val());
            price = get_price_value($select_product_price.val());

            $input_price_value.val(price);
            $input_price_name.val(price_name);
            $text_price.text(price);
        });

        // set price name on load if price type is variation
        price_name = get_price_name($select_product_price.val());
        price = get_price_value($select_product_price.val());

        $input_price_name.val(price_name);
        $input_price_value.val(price);
        $text_price.text(price);
    }
    else {

        // console.log('one off charge', )
        // set price name on load if price type is singular
        $input_price_value.val($product_price.val());
        $text_price.text($product_price.val());
    }

    function get_price_name(value)
    {
        return value.split(' - ')[0];
    }

    function get_price_value(value)
    {
        return value.split(' - ')[1];
    }
</script>
@endsection