@extends('layouts.auth_all')


@section('title')
Checkout Failed
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">

        <h3>Checkout Failed!</h3>

        <p>You are still currently subscribed in this product.</p>

    </div>
@endsection


@section('script-footer')
@endsection