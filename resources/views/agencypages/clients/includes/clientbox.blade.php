<div class="item">
  <div class="right floated content icon">
    <div class="ui text menu">
      <div class="ui button">Message</div>
      <div class="ui top right pointing right dropdown item">
        <i class="large ellipsis horizontal icon"></i>
        <div class="menu">
          <div class="item"><i class="building icon"></i>Profile</div>
          <div class="item"><i class="edit icon"></i>Edit</div>
          <div class="item"><i class="red times icon"></i>Remove</div>
        </div>
      </div>
    </div>
  </div>
  <i class="huge building icon"></i>
  <div class="content">
    <a class="header">Institution Name</a>
    <div class="description">Institution Type</div>
  </div>
</div>