<form id="form_loader" class="ui form {{ ($errors->any()) ? 'error' : '' }}" method="post" action="{{ route('agency.clients.invite.store') }}" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="two fields">
    <div class="field">
      <label>Contact Name</label>
      <div class="ui left icon input">
        <input type="text" name="contact_name" placeholder="Contact Name">
        <i class="user icon"></i>
      </div>
    </div>
    <div class="field">
      <label>Email</label>
      <div class="ui left icon input">
        <input type="text" name="email" placeholder="Email" required>
        <i class="envelope icon"></i>
      </div>
    </div>
  </div>
  <div class="field">
    <label>Institution Name</label>
    <div class="ui left icon input">
      <input type="text" name="institution_name" placeholder="Institution Name">
      <i class="building icon"></i>
    </div>
  </div>
  <div class="field">
    <label>Message (optional)</label>
    <textarea name="message"></textarea>
  </div>

  @if($errors->any())
    <div class="ui error message">
      <div class="header">There were some errors with your invitation</div>
      <ul class="list">
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <button class="ui button" type="submit">Send Invitation</button>
</form>