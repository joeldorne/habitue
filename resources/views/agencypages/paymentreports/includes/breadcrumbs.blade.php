@if (\Request::route()->getName() == 'agency.paymentreport')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="dollar sign icon"></i> 
        Payment Reports 
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'agency.paymentreport.purchases')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="dollar sign icon"></i> 
        <a class="section" href="/a/paymentreports">Payment Reports</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        Products Sold
      </div>
    </div>
  </div>

@endif