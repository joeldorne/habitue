@extends('layouts.auth_all')


@section('title')
Products Sold
@endsection


@section('script-header')
@endsection


@section('content')
  @include('agencypages.paymentreports.includes.breadcrumbs')


  <products-sold-list :productssold="{{ $products_sold }}"></products-sold-list>


@endsection


@section('script-footer')
@endsection