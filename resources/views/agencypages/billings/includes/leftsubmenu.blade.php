<div class="ui vertical menu m-t-30">
  <div class="item">
    <div class="header">Profile</div>
    <div class="menu">
      <a class="item">Billing Information</a>
    </div>
  </div>
  <div class="item">
    <div class="header">Payment Bills</div>
    <div class="menu">
      <a class="item {{ toggleClass(\Request::route()->getName(), 'agency.billings', 'active') }}" href="/a/billings">Created Bills</a>
      <a class="item">Pending Payments</a>
      <a class="item">Paid Bills</a>
    </div>
  </div>
  <div class="item">
    <div class="header">Client Cards</div>
    <div class="menu">
      <a class="item {{ toggleClass(\Request::route()->getName(), 'agency.billings.cards', 'active') }}" href="/a/billings/cards">Stored Cards</a>
    </div>
  </div>
</div>