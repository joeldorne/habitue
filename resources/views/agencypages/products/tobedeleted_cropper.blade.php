@extends('layouts.agency-dashboard')


@section('title')
Cropper
@endsection


@section('script-header')
  <link href="{{URL::asset('vendor/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')

  <div class="ui main fluidx container segment basic">
    <div class="docs-preview clearfix" style="width: 300px; height:150px">
      <div class="img-preview preview-xs" style="width: 300px; height:150px"></div>
    </div>
    <div style="width: 600px; height:300px">
      <img id="image_cropper" src="/images/semantics/avatar/large/daniel.jpg">
    </div>
  </div>

  <div>

    <div class="btn-group">
      <button type="button" id="drag" class="btn btn-primary" data-method="drag" data-option="0.1" title="drag">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="">
          drag mode
        </span>
      </button>
    </div>

    <div class="btn-group">
      <button type="button" id="zoomin" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, 0.1)">
          zoom in
        </span>
      </button>
      <button type="button" id="zoomout" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;zoom&quot;, -0.1)">
          zoom out
        </span>
      </button>
    </div>

    <div class="btn-group">
      <button type="button" id="rotate_left" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, -45)">
          rotate left
        </span>
      </button>
      <button type="button" id="rotate_right" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;rotate&quot;, 45)">
          rotate right
        </span>
      </button>
    </div>

    <div class="btn-group">
      <button type="button" id="scaleX" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleX&quot;, -1)">
          flip horizontal
        </span>
      </button>
      <button type="button" id="scaleY" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;scaleY&quot;, -1)">
          flip vertical
        </span>
      </button>
    </div>

    <div class="btn-group">
      <button type="button" id="get-image" class="btn btn-primary" data-method="get-image" data-option="-1" title="saget-imageve">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" >
          get image
        </span>
      </button>
      <button type="button" id="save" class="btn btn-primary" data-method="save" data-option="-1" title="save">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" >
          save
        </span>
      </button>
    </div>

    <div class="ui main fluidx container segment basic">
      <img id="image_source" src="">
    </div>
    
  </div>
@endsection


@section('script-footer')
  <script src="{{URL::asset('vendor/cropper/cropper.min.js')}}"></script>
  <!--<script src="{{URL::asset('js/temp_cropper_init.js')}}"></script>-->

  <script>
    var $image = $('#image_cropper');

    var options = {
      // aspectRatio: 16 / 9,
      // preview: '.img-preview',
      aspectRatio: 1 / 1,
      minContainerWidth: 570,
      minContainerHeight: 350,
      minCropBoxWidth: 150,
      minCropBoxHeight: 150,
      rotatable: true,
      cropBoxResizable: true,
      crop: function (e) {
        // $dataX.val(Math.round(e.detail.x));
        // $dataY.val(Math.round(e.detail.y));
        // $dataHeight.val(Math.round(e.detail.height));
        // $dataWidth.val(Math.round(e.detail.width));
        // $dataRotate.val(e.detail.rotate);
        // $dataScaleX.val(e.detail.scaleX);
        // $dataScaleY.val(e.detail.scaleY);
      }
    };

    $image.cropper(options);

    // Get the Cropper.js instance after initialized
    var cropper = $image.data('cropper');

    $('#zoomin').click(function(){
      $image.cropper('zoom', 0.1);
    });

    $('#zoomout').click(function(){
      $image.cropper('zoom', -0.1);
    });

    $('#drag').click(function(){
      $image.cropper('setDragMode', 'move');
    });

    $('#rotate_left').click(function(){
      $image.cropper('rotate', -45);
    });

    $('#rotate_right').click(function(){
      $image.cropper('rotate', 45);
    });

    var scaleX = 1;
    $('#scaleX').click(function(){
      scaleX *= -1;
      $image.cropper('scaleX', scaleX);
    });

    var scaleY = 1;
    $('#scaleY').click(function(){
      scaleY *= -1;
      $image.cropper('scaleY', scaleY);
    });

    $('#get-image').click(function(){
      var orig_image = $image.cropper('getImageData');
      var canvasURL = orig_image.toDataURL('image/jpeg');
      $("#image_source").attr('src', canvasURL);
    });

    var formData = new FormData();   
    $('#save').click(function(){
      var cropped_image = $image.cropper('getCroppedCanvas');
      var cropped_image_data = cropped_image.toDataURL('image/jpeg');
      $("#image_source").attr('src', cropped_image_data);

        // var blob = cropper.getCroppedCanvas().toBlob(function (blob) 
        // {
          // return blob;

          // formData.append('croppedImage', blob);
          // console.log(formData);
        // });

return;
      console.log('1',formData);
      getBlob();

      console.log('5',formData);

      axios.post('/agency/cropper', 
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        .then(response => {
          console.log('axios: ', response.data);
        })
        .catch(error => {
          console.log(error.response.data.errors);
        });
      
    });

    function getBlob() 
    {
      console.log('2',formData);
      cropper.getCroppedCanvas().toBlob(function (blob) 
      {
        console.log('3',formData);
        formData.append('croppedImage', blob);
        console.log('4',formData);
      });
    }
  </script>
@endsection