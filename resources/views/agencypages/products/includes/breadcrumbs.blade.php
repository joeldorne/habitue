@if (\Request::route()->getName() == 'agency.products')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        Products 
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'agency.products.create')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="/a/products">Products</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Create New Product
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'agency.products.edit')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="/a/products">Products</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Edit Product
      </div>
    </div>
  </div>

@endif