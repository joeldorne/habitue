@extends('layouts.auth_all')


@section('title')
Invite Member - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div id="form_loader" class="ui main segment basic">
    @include('agencypages.teams.includes.header')
    
    <div class="ui stackable grid">
      <div class="eight wide column">
        <h3 class="ui header">
          <div class="content">
            Invite new member
            <div class="sub header">Invite new member by sending an email invitation.</div>
          </div>
        </h3>        

        <div class="ui form {{ ($errors->any()) ? 'error' : '' }}">
          <form class="loading" method="post" action="{{ route('agency.team.invites.store') }}">
            {{ csrf_field() }}

            <div class="field">
              <label>E-mail</label>
              <input type="email" name="email" placeholder="john@doe.com" value="{{ old('email') }}" required>
            </div>
            <div class="ui success message">
              <div class="header">Invite Submitted</div>
              <p>An email have been succesfully sent to invite the new member.</p>
            </div>

            @if($errors->any())
              <div class="ui error message">
                <div class="header">There were some errors with your invitation</div>
                <ul class="list">
                  @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            <input type="submit" id="btn_submit" class="ui submit button" value="Invite">
          </form>
        </div>
      </div>

      <div class="eight wide column">
        @if (session('success'))
          <div class="ui success message">
            <div class="header">Success</div>
            <p>{{ session('success') }}</p>
          </div>
        @endif

        @if (session('error'))
          <div class="ui error message">
            <div class="header">Failed</div>
            <p>{{ session('error') }}</p>
          </div>
        @endif

        <h4 class="ui header">Invited Email:</h4>

        @if ($invites->count() > 0)
          <div class="ui bulleted list">
            @foreach($invites->reverse() as $invite)
              <div class="item">
                <div class="content">
                  <div class="header">
                    {{ $invite->email }} 
                  </div>
                  <div class="small description">{{ $invite->updated_at->format('M d, Y - h:i:s A') }}</div>
                  <div>
                    <a href="{{ route('agency.team.invites.resendemail', $invite->email) }}" data-tooltip="resend email to {{ $invite->email }}" data-position="top center">
                      <i class="mail icon"></i>
                    </a>
                    <a href="{{ route('agency.team.invites.remove', $invite->email) }}" data-tooltip="recede invitation to {{ $invite->email }}" data-position="top center">
                      <i class="times red icon"></i>
                    </a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        @else
          <div class="ui bulleted list">
            <div class="item">
              <div class="content">
                <div class="small description">
                  No invitation found. 
                </div>
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>

  </div>
@endsection


@section('script-footer')
  <script>
    $(document).ready(function () {
      $('#form_loader').submit(function(e){
        $(this).addClass('loading');
      });
    });
  </script>
@endsection