@extends('layouts.auth_all')


@section('title')
Members - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui form_loader main fluid segment basic">

    <div class="ui stackable grid">
      <div class="twelve wide column">
        @include('agencypages.teams.includes.header')

        @include('agencypages.teams.includes.memberlist')
      </div>

      <div class="four wide column">
        @include('agencypages.teams.includes.invitelist')
      </div>
    </div>

  </div>
@endsection


@section('script-footer')
  <script>
    form_loader('#form_team_invite','.form_loader');
  </script>
@endsection