@extends('layouts.auth_all')


@section('title')
Billings
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main segment basic">
    <div class="ui stackable grid">

      <div class="three wide column">
        <div class="ui breadcrumb">
          <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
          <span class="divider">/</span>
          <div class="active section">Billings</div>
        </div>

        @include('clientpages.billings.includes.leftsubmenu')
      </div>


      <div class="thirteen wide column">

        <!-- Event ERROR Messages -->
        @if (session('error'))
          <div class="ui small icon orange message container">
            <i class="exclamation icon"></i>

            <div class="content">

              <p>{{ session('error') }}</p>
            </div>
          </div>
        @endif


        @if ($customer)
          @if ($customer->sources->total_count > 0)
          

            <!-- Card list -->
            <div class="ui stackable grid">
              @foreach ($customer->sources->data as $card)
                @include('clientpages.billings.includes.credit-card-box', ['card'=>$card, 'default_card'=>$customer->default_source])
              @endforeach

              <div class="five wide column">
                <div class="ui divided items">
                  <div class="huge icon item">
                    <div class="content">
                      <div class="meta">
                        <a href="{{ route('client.billings.cards.add') }}">
                          <i class="plus huge icon"></i>
                        </a>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>

            </div>


          @else


            <!-- Empty Card Message -->
            <div class="ui small message">
              <p>You haven't added any cards yet. <a href="{{route('client.billings.cards.add')}}">Add new card</a></p>
            </div>


          @endif
        @else


          <!-- Stripe Server ERROR Message -->
          <div class="ui big icon negative message">
            <i class="exclamation icon"></i>

            <div class="content">
              <div class="header">
                Oops!
              </div>

              <div class="header">
                Looks like Stripe server is having issues! 
              </div>

              <p>Please give it another try by refreshing the page. If this persist contact the site Administrator.</p>
            </div>
          </div>


        @endif
      </div>

    </div>
  </div>
@endsection


@section('script-footer')
@endsection