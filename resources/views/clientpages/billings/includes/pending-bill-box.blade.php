<div class="card">
  <div class="content">
    $ 334.00
  </div>

  <div class="content">
    <div class="header">
      Client Name
    </div>
    <div class="meta">
      more details
    </div>
    <div class="description">
      Payments for ... details ...
    </div>
  </div>
  
  <div class="extra content">
      <div class="ui right floated primary button">Pay Now</div>
  </div>
</div>