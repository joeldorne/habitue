@extends('layouts.auth_all')


@section('title')
Billings
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main segment basic">
    
    <div class="ui stackable grid">

      <div class="three wide column">
        <div class="ui breadcrumb">
          <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
          <span class="divider">/</span>
          <div class="active section">Billings</div>
        </div>

        @include('clientpages.billings.includes.leftsubmenu')
      </div>

      <div class="thirteen wide column">

        <div class="ui cards">
          @include('clientpages.billings.includes.pending-bill-box')
        </div>

      </div>
    </div>

  </div>
@endsection


@section('script-footer')
@endsection