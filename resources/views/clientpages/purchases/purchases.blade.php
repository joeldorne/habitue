@extends('layouts.auth_all')


@section('title')
Purchases
@endsection


@section('script-header')
@endsection


@section('content')
  @include('clientpages.purchases.includes.header')


  <products-purchases-list :productspurchased="{{ $purchases }}"></products-purchases-list>


@endsection


@section('script-footer')
@endsection