@extends('layouts.auth_all')


@section('title')
Members
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui form_loader main fluid segment basic">

    <div class="ui stackable grid">
      <div class="twelve wide column">
        @include('clientpages.teams.includes.header')

        @include('clientpages.teams.includes.memberlist')
      </div>

      <div class="four wide column">
        @include('clientpages.teams.includes.invitelist')
      </div>
    </div>

  </div>
@endsection


@section('script-footer')
  <script>
    $(document).ready(function () {
      let $box_loader = $('.form_loader');
      let $form_trigger = $('#form_team_invite');

      $form_trigger.submit(function(e){
        $box_loader.addClass('loading');
      });
    });
  </script>
@endsection