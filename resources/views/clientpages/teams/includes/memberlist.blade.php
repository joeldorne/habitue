@if (Auth::user()->client[0]->members->count())
  <div class="ui segment basic grid">
    <div class="ui horizontal list">
      @foreach (Auth::user()->client[0]->members as $member)
        <div class="item">
          <img class="ui mini circular image" src="/storage/{{ $member->avatar }}">
          <div class="content">
            <div class="ui sub header">{{ $member->name }}</div>
            <span>{{ $member->pivot->position }}</span>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@else
  <div class="ui negative message">
    <i class="close icon"></i>
    <div class="header">
      This is strange! There is no member, that means you're not suppose to be in this page either.
    </div>
    <p>This is a bug. Please report to admin.</p>
  </div>
@endif