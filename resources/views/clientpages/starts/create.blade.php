@extends('layouts.auth_all')


@section('title')
Create Group
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main container segment basic center aligned">
    <h3 class="ui">Welcome to Habitue, {{ Auth::user()->fullname }}</h3>

    <p class="ui">To start your first project you first need to create your group profile.</p>
  </div>

  <div class="ui main container segment basic">
    @if($errors->any())
      <div class="ui error message">
        <i class="close icon"></i>
        <div class="header">
          There were some errors with your submission
        </div>
        <ul class="list">
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <div id="form_loader" class="ui form">
      <form id="from_create" method="post" action="{{ route('client.store') }}">
        {{ csrf_field() }}
        <div class="three fields">
          <div class="field"></div>
          <div class="required field">
            <label>Group name</label>
            <input type="text" name="name" placeholder="the name of your group" value="{{ old('name') }}" required>
          </div>
          <div class="field"></div>
        </div>

        <div class="three fields">
          <div class="field"></div>
          <div class="field">
            <label>Phone</label>
            <input type="text" name="phone" placeholder="phone" value="{{ old('phone') }}">
          </div>
          <div class="field"></div>
        </div>

        <div class="three fields">
          <div class="field"></div>
          <div class="field">
            <label>Website</label>
            <div class="ui labeled input">
              <div class="ui label">
                http://
              </div>
              <input type="text" name="website" placeholder="website" value="{{ old('website') }}">
            </div>
          </div>
          <div class="field"></div>
        </div>

        <div class="three fields">
          <div class="field"></div>
          <div class="field">
            <label>Country</label>
            @include('components.country-select-plain')
          </div>
          <div class="field"></div>
        </div>

        <div class="three fields">
          <div class="field"></div>
          <div class="field">
            <label>Subdomain</label>
            <div class="ui labeled input">
              <input type="text" name="subdomain" placeholder="subdomain" value="{{ old('subdomain') }}" maxlength="15" required>
              <div class="ui label">
                .habitue.com
              </div>
            </div>
          </div>
          <div class="field"></div>
        </div>

        <div class="three fields">
          <div class="field"></div>
          <div class="field">
            <input type="submit" id="btn_submit" class="ui submit orange huge button" value="Create Your Agency">
          </div>
          <div class="field"></div>
        </div>
      </form>
    </div>

  </div>
@endsection


@section('script-footer')
  <script>
    form_loader('#from_create','#form_loader');
  </script>

  <script>
    $(document).ready(function () {

      let $subdomain = $('[name="subdomain"]');

      let subdomain_touched = false;


      if (!$subdomain.val())
        subdomain_touched = false;
      else
        subdomain_touched = true;

      

      $('[name="name"]').keyup(function() {

        if (subdomain_touched == false) {

          let $name = $('[name="name"]');

          $subdomain.val( $name.val().trim().replace(/[^a-z]/gi,'') );



          if ($subdomain.val().length > 15) {

            $subdomain.val( $subdomain.val().substring(0, 15) );

          }

        }

      });



      $subdomain.keyup(function() {

        subdomain_touched = true;

        $subdomain.val( $subdomain.val().replace(/[^a-z]/gi,'') );

      });
    });
  </script>
@endsection