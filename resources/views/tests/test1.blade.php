<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
  </head>

  <body id="app">
    <!--  Side Bar -->
    <div class="ui visible inverted left vertical sidebar menu very thin icon">
      <a class="item"><i class="folder icon"></i></a>
      <a class="item"><i class="archive icon"></i></a>
      <a class="item"><i class="calendar outline icon"></i></a>
    </div>

    <div class="mypusher" style="margin-left:60px;">
      <!-- Start of Menu -->
      <div class="ui large menu attached">
        <div class="header item">
          Habitue
        </div>

        <div class="right menu">
          <a class="item">Sign Up</a>
          <a class="item">Help</a>
        </div>
      </div>
      <!-- End of menu -->


      <div class="ui grid padded ">
        <div class="column">
          <h1 class="ui header">Dashboard</h1>
        </div>
      </div>


      <div class="ui grid padded stackable ">
        <div class="four wide column">

          <div class="ui card">
            <div class="content">
              <div class="header">Project Timeline</div>
            </div>

            <div class="content">
              <h4 class="ui sub header">Activity</h4>

              <div class="ui small feed">
                <div class="event">
                  <div class="content">
                    <div class="summary">
                      <a>Elliot Fu</a> added <a>Jenny Hess</a> to the project
                    </div>
                  </div>
                </div>

                <div class="event">
                  <div class="content">
                    <div class="summary">
                      <a>Stevie Feliciano</a> was added as an <a>Administrator</a>
                    </div>
                  </div>
                </div>

                <div class="event">
                  <div class="content">
                    <div class="summary">
                      <a>Helen Troy</a> added two pictures
                    </div>
                  </div>
                </div>
              </div>
          </div>

          <div class="extra content">
            <button class="ui button">Join Project</button>
          </div>
        
        </div>
      </div>


      <div class="four wide column">
        <div class="ui card">
          <div class="content">
            <div class="header">Project Timeline</div>
          </div>

          <div class="content">
            <h4 class="ui sub header">Activity</h4>

            <div class="ui small feed">
              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Elliot Fu</a> added <a>Jenny Hess</a> to the project
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Stevie Feliciano</a> was added as an <a>Administrator</a>
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Helen Troy</a> added two pictures
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="extra content">
            <button class="ui button">Join Project</button>
          </div>
        </div>
      </div>


      <div class="four wide column">
        <div class="ui card">
          <div class="content">
            <div class="header">Project Timeline</div>
          </div>

          <div class="content">
            <h4 class="ui sub header">Activity</h4>

            <div class="ui small feed">
              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Elliot Fu</a> added <a>Jenny Hess</a> to the project
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Stevie Feliciano</a> was added as an <a>Administrator</a>
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Helen Troy</a> added two pictures
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="extra content">
            <button class="ui button">Join Project</button>
          </div>
        </div>
      </div>


      <div class="four wide column">
        <div class="ui card">
          <div class="content">
            <div class="header">Project Timeline</div>
          </div>

          <div class="content">
            <h4 class="ui sub header">Activity</h4>

            <div class="ui small feed">
              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Elliot Fu</a> added <a>Jenny Hess</a> to the project
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Stevie Feliciano</a> was added as an <a>Administrator</a>
                  </div>
                </div>
              </div>

              <div class="event">
                <div class="content">
                  <div class="summary">
                    <a>Helen Troy</a> added two pictures
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="extra content">
            <button class="ui button">Join Project</button>
          </div>
        </div>
      </div>
    </div> <!-- End MyPusher -->

    <div class="ui grid padded">
      
    </div>
  </div>
</html>