<table class="ui padded stackable table">
    <thead class="full-width">
        <tr class="warning">
            <th colspan="5">
                <a href="{{ route('admin.subscription.create') }}" class="ui right floated small primary labeled icon button">
                    <i class="plus icon"></i> Add Subscription
                </a>
            </th>
        </tr>
    </thead>

    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price(s)</th>
            <th class="center aligned"><i class="cog icon"></i></th>
        </tr>
    </thead>
    
    




    <tbody>

        @foreach ($subscriptions as $subscription)
            <tr>

                <td>
                    <h4 class="ui image header">

                        @if(file_exists( $subscription->image )) 
                            <img src="/{{ $subscription->image }}" class="ui mini rounded image">
                        @else
                            <img src="/storage/subscriptions/default.png" class="ui mini rounded image">
                        @endif

                        <div class="content">
                            {{ $subscription->name }}
                            <div class="sub">
                                @if ( $subscription->disabled == 1 )
                                    <span class="ui red small label">inactive</span>
                                @endif
                            </div>
                        </div>

                    </h4>
                </td>

                <td>{{ $subscription->description }}</td>

                <td></td>

                <td class="center aligned">
                    <div class="ui dropdown item">

                        <i class="grey ellipsis horizontal icon"></i>

                        <div class="menu">
                            <a href="{{ route('admin.subscription.view', $subscription->slug) }}" class="green item">View</a>
                            <a href="{{ route('admin.subscription.edit', $subscription->slug) }}" class="green item">Edit</a>
                            <a class="red item">Permanently Remove</a>
                        </div>

                    </div>
                </td>

            </tr>
        @endforeach

    </tbody>
</table>