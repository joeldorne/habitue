@if (\Request::route()->getName() == 'admin.subscriptions')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        Subscriptions 
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.subscription.create')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.subscriptions') }}">Subscriptions</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Create New Subscription
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.subscription.view')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.subscriptions') }}">Subscriptions</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        View Subscription: {{ $product_service->name }}
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.subscription.edit')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.subscriptions') }}">Subscriptions</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Edit Subscription: {{ $product_service->name }}
      </div>
    </div>
  </div>

@endif