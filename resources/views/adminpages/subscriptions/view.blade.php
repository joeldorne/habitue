@extends('layouts.auth_all')


@section('title')
Subscription: {{ $product_service->name }}
@endsection


@section('script-header')
@endsection


@section('content')
    @include('adminpages.subscriptions.partials.breadcrumbs')

    <div class="ui main fluidx container segment basic">


        <subscription-crud-container :receivingsubscription="{{$product_service}}" :setmode="'view'"></subscription-crud-container>


    </div>
@endsection


@section('script-footer')
@endsection