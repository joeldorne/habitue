@extends('layouts.auth_all')


@section('title')
Create New Subscription
@endsection


@section('script-header')
@endsection


@section('content')
    @include('adminpages.subscriptions.partials.breadcrumbs')

    <div class="ui main fluidx container segment basic">

        <subscription-crud-container :setmode="'create'"></subscription-crud-container>

    </div>
@endsection


@section('script-footer')
@endsection