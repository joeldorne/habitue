<table class="ui padded stackable table">
    <thead class="full-width">
        <tr class="warning">
            <th colspan="5">
                <a href="{{ route('admin.product.create') }}" class="ui right floated small primary labeled icon button">
                    <i class="plus icon"></i> Add Product
                </a>
            </th>
        </tr>
    </thead>

    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price(s)</th>
            <th class="center aligned"><i class="cog icon"></i></th>
        </tr>
    </thead>
    
    




    <tbody>

        @foreach ($products->reverse() as $product)
            <tr>

                <td>
                    <h4 class="ui image header">

                        @if(file_exists( $product->image )) 
                            <img src="/{{ $product->image }}" class="ui mini rounded image">
                        @else
                            <img src="/storage/products/default.png" class="ui mini rounded image">
                        @endif

                        <div class="content">
                            {{ $product->name }}
                            <div class="sub">
                                @if ( $product->disabled == 1 )
                                    <span class="ui red small label">inactive</span>
                                @endif
                                @if ( $product->payment_method == 'subscription' )
                                    <span class="ui small label">subscription</span>
                                @endif
                            </div>
                        </div>

                    </h4>
                </td>

                <td>{{ $product->description }}</td>

                @if ($product->price_type == 'singular')
                    <td>$ {{ $product->singular_price }}</td>
                @else
                    <td> {{ get_data_from_variation_prices($product->variation_prices) }} </td>
                @endif

                <td class="center aligned">
                    <div class="ui dropdown item">

                        <i class="grey ellipsis horizontal icon"></i>

                        <div class="menu">
                            <a href="{{ route('admin.product.view', $product->slug) }}" class="green item">View</a>
                            <a href="{{ route('admin.product.edit', $product->slug) }}" class="green item">Edit</a>
                            @if ( $product->disabled == 1 )
                                <a href="{{ route('admin.product.settoactive', $product->slug) }}" class="item">Set this Active</a>
                            @else
                                <a href="{{ route('admin.product.settoinactive', $product->slug) }}" class="item">Set this Inactive</a>
                            @endif
                            <a class="red item">Permanently Remove</a>
                        </div>

                    </div>
                </td>

            </tr>
        @endforeach

    </tfoot>
</table>