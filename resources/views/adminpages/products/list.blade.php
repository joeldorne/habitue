@extends('layouts.auth_all')


@section('title')
Products
@endsection


@section('script-header')
@endsection


@section('content')
    @include('adminpages.products.partials.breadcrumbs')

    <div class="ui main fluid container segment basic">

    
        @include('adminpages.products.partials.product-table-list', $products)


    </div>
@endsection


@section('script-footer')
@endsection