@extends('layouts.auth_all')


@section('title')
Payment Reports
@endsection


@section('script-header')
@endsection


@section('content')
    <!-- breadcrumbs here -->

    <div class="ui main fluid container segment basic">
        <h3>Payment Reports</h3>
        
        <products-sold-list v-bind:purchases="{{ $purchases }}"></products-sold-list>

    </div>
@endsection


@section('script-footer')
@endsection
