@php
  if ( !isset($selected) )
    $selected = '';
@endphp

<select name="country" class="ui search dropdown" value="{{ $selected }}">
  <option value="">Select Country</option>
  <option value="AF" {{ ($selected == 'AF')?'selected':'' }}>Afghanistan</option>
  <option value="AX" {{ ($selected == 'AX')?'selected':'' }}>Åland Islands</option>
  <option value="AL" {{ ($selected == 'AL')?'selected':'' }}>Albania</option>
  <option value="DZ" {{ ($selected == 'DZ')?'selected':'' }}>Algeria</option>
  <option value="AS" {{ ($selected == 'AS')?'selected':'' }}>American Samoa</option>
  <option value="AD" {{ ($selected == 'AD')?'selected':'' }}>Andorra</option>
  <option value="AO" {{ ($selected == 'AO')?'selected':'' }}>Angola</option>
  <option value="AI" {{ ($selected == 'AI')?'selected':'' }}>Anguilla</option>
  <option value="AQ" {{ ($selected == 'AQ')?'selected':'' }}>Antarctica</option>
  <option value="AG" {{ ($selected == 'AG')?'selected':'' }}>Antigua and Barbuda</option>
  <option value="AR" {{ ($selected == 'AR')?'selected':'' }}>Argentina</option>
  <option value="AM" {{ ($selected == 'AM')?'selected':'' }}>Armenia</option>
  <option value="AW" {{ ($selected == 'AW')?'selected':'' }}>Aruba</option>
  <option value="AU" {{ ($selected == 'AU')?'selected':'' }}>Australia</option>
  <option value="AT" {{ ($selected == 'AT')?'selected':'' }}>Austria</option>
  <option value="AZ" {{ ($selected == 'AZ')?'selected':'' }}>Azerbaijan</option>
  <option value="BS" {{ ($selected == 'BS')?'selected':'' }}>Bahamas</option>
  <option value="BH" {{ ($selected == 'BH')?'selected':'' }}>Bahrain</option>
  <option value="BD" {{ ($selected == 'BD')?'selected':'' }}>Bangladesh</option>
  <option value="BB" {{ ($selected == 'BB')?'selected':'' }}>Barbados</option>
  <option value="BY" {{ ($selected == 'BY')?'selected':'' }}>Belarus</option>
  <option value="BE" {{ ($selected == 'BE')?'selected':'' }}>Belgium</option>
  <option value="BZ" {{ ($selected == 'BZ')?'selected':'' }}>Belize</option>
  <option value="BJ" {{ ($selected == 'BJ')?'selected':'' }}>Benin</option>
  <option value="BM" {{ ($selected == 'BM')?'selected':'' }}>Bermuda</option>
  <option value="BT" {{ ($selected == 'BT')?'selected':'' }}>Bhutan</option>
  <option value="BO" {{ ($selected == 'BO')?'selected':'' }}>Bolivia, Plurinational State of</option>
  <option value="BQ" {{ ($selected == 'BQ')?'selected':'' }}>Bonaire, Sint Eustatius and Saba</option>
  <option value="BA" {{ ($selected == 'BA')?'selected':'' }}>Bosnia and Herzegovina</option>
  <option value="BW" {{ ($selected == 'BW')?'selected':'' }}>Botswana</option>
  <option value="BV" {{ ($selected == 'BV')?'selected':'' }}>Bouvet Island</option>
  <option value="BR" {{ ($selected == 'BR')?'selected':'' }}>Brazil</option>
  <option value="IO" {{ ($selected == 'IO')?'selected':'' }}>British Indian Ocean Territory</option>
  <option value="BN" {{ ($selected == 'BN')?'selected':'' }}>Brunei Darussalam</option>
  <option value="BG" {{ ($selected == 'BG')?'selected':'' }}>Bulgaria</option>
  <option value="BF" {{ ($selected == 'BF')?'selected':'' }}>Burkina Faso</option>
  <option value="BI" {{ ($selected == 'BI')?'selected':'' }}>Burundi</option>
  <option value="KH" {{ ($selected == 'KH')?'selected':'' }}>Cambodia</option>
  <option value="CM" {{ ($selected == 'CM')?'selected':'' }}>Cameroon</option>
  <option value="CA" {{ ($selected == 'CA')?'selected':'' }}>Canada</option>
  <option value="CV" {{ ($selected == 'CV')?'selected':'' }}>Cape Verde</option>
  <option value="KY" {{ ($selected == 'KY')?'selected':'' }}>Cayman Islands</option>
  <option value="CF" {{ ($selected == 'CF')?'selected':'' }}>Central African Republic</option>
  <option value="TD" {{ ($selected == 'TD')?'selected':'' }}>Chad</option>
  <option value="CL" {{ ($selected == 'CL')?'selected':'' }}>Chile</option>
  <option value="CN" {{ ($selected == 'CN')?'selected':'' }}>China</option>
  <option value="CX" {{ ($selected == 'CX')?'selected':'' }}>Christmas Island</option>
  <option value="CC" {{ ($selected == 'CC')?'selected':'' }}>Cocos (Keeling) Islands</option>
  <option value="CO" {{ ($selected == 'CO')?'selected':'' }}>Colombia</option>
  <option value="KM" {{ ($selected == 'KM')?'selected':'' }}>Comoros</option>
  <option value="CG" {{ ($selected == 'CG')?'selected':'' }}>Congo</option>
  <option value="CD" {{ ($selected == 'CD')?'selected':'' }}>Congo, the Democratic Republic of the</option>
  <option value="CK" {{ ($selected == 'CK')?'selected':'' }}>Cook Islands</option>
  <option value="CR" {{ ($selected == 'CR')?'selected':'' }}>Costa Rica</option>
  <option value="CI" {{ ($selected == 'CI')?'selected':'' }}>Côte d'Ivoire</option>
  <option value="HR" {{ ($selected == 'HR')?'selected':'' }}>Croatia</option>
  <option value="CU" {{ ($selected == 'CU')?'selected':'' }}>Cuba</option>
  <option value="CW" {{ ($selected == 'CW')?'selected':'' }}>Curaçao</option>
  <option value="CY" {{ ($selected == 'CY')?'selected':'' }}>Cyprus</option>
  <option value="CZ" {{ ($selected == 'CZ')?'selected':'' }}>Czech Republic</option>
  <option value="DK" {{ ($selected == 'DK')?'selected':'' }}>Denmark</option>
  <option value="DJ" {{ ($selected == 'DJ')?'selected':'' }}>Djibouti</option>
  <option value="DM" {{ ($selected == 'DM')?'selected':'' }}>Dominica</option>
  <option value="DO" {{ ($selected == 'DO')?'selected':'' }}>Dominican Republic</option>
  <option value="EC" {{ ($selected == 'EC')?'selected':'' }}>Ecuador</option>
  <option value="EG" {{ ($selected == 'EG')?'selected':'' }}>Egypt</option>
  <option value="SV" {{ ($selected == 'SV')?'selected':'' }}>El Salvador</option>
  <option value="GQ" {{ ($selected == 'GQ')?'selected':'' }}>Equatorial Guinea</option>
  <option value="ER" {{ ($selected == 'ER')?'selected':'' }}>Eritrea</option>
  <option value="EE" {{ ($selected == 'EE')?'selected':'' }}>Estonia</option>
  <option value="ET" {{ ($selected == 'ET')?'selected':'' }}>Ethiopia</option>
  <option value="FK" {{ ($selected == 'FK')?'selected':'' }}>Falkland Islands (Malvinas)</option>
  <option value="FO" {{ ($selected == 'FO')?'selected':'' }}>Faroe Islands</option>
  <option value="FJ" {{ ($selected == 'FJ')?'selected':'' }}>Fiji</option>
  <option value="FI" {{ ($selected == 'FI')?'selected':'' }}>Finland</option>
  <option value="FR" {{ ($selected == 'FR')?'selected':'' }}>France</option>
  <option value="GF" {{ ($selected == 'GF')?'selected':'' }}>French Guiana</option>
  <option value="PF" {{ ($selected == 'PF')?'selected':'' }}>French Polynesia</option>
  <option value="TF" {{ ($selected == 'TF')?'selected':'' }}>French Southern Territories</option>
  <option value="GA" {{ ($selected == 'GA')?'selected':'' }}>Gabon</option>
  <option value="GM" {{ ($selected == 'GM')?'selected':'' }}>Gambia</option>
  <option value="GE" {{ ($selected == 'GE')?'selected':'' }}>Georgia</option>
  <option value="DE" {{ ($selected == 'DE')?'selected':'' }}>Germany</option>
  <option value="GH" {{ ($selected == 'GH')?'selected':'' }}>Ghana</option>
  <option value="GI" {{ ($selected == 'GI')?'selected':'' }}>Gibraltar</option>
  <option value="GR" {{ ($selected == 'GR')?'selected':'' }}>Greece</option>
  <option value="GL" {{ ($selected == 'GL')?'selected':'' }}>Greenland</option>
  <option value="GD" {{ ($selected == 'GD')?'selected':'' }}>Grenada</option>
  <option value="GP" {{ ($selected == 'GP')?'selected':'' }}>Guadeloupe</option>
  <option value="GU" {{ ($selected == 'GU')?'selected':'' }}>Guam</option>
  <option value="GT" {{ ($selected == 'GT')?'selected':'' }}>Guatemala</option>
  <option value="GG" {{ ($selected == 'GG')?'selected':'' }}>Guernsey</option>
  <option value="GN" {{ ($selected == 'GN')?'selected':'' }}>Guinea</option>
  <option value="GW" {{ ($selected == 'GW')?'selected':'' }}>Guinea-Bissau</option>
  <option value="GY" {{ ($selected == 'GY')?'selected':'' }}>Guyana</option>
  <option value="HT" {{ ($selected == 'HT')?'selected':'' }}>Haiti</option>
  <option value="HM" {{ ($selected == 'HM')?'selected':'' }}>Heard Island and McDonald Islands</option>
  <option value="VA" {{ ($selected == 'VA')?'selected':'' }}>Holy See (Vatican City State)</option>
  <option value="HN" {{ ($selected == 'HN')?'selected':'' }}>Honduras</option>
  <option value="HK" {{ ($selected == 'HK')?'selected':'' }}>Hong Kong</option>
  <option value="HU" {{ ($selected == 'HU')?'selected':'' }}>Hungary</option>
  <option value="IS" {{ ($selected == 'IS')?'selected':'' }}>Iceland</option>
  <option value="IN" {{ ($selected == 'IN')?'selected':'' }}>India</option>
  <option value="ID" {{ ($selected == 'ID')?'selected':'' }}>Indonesia</option>
  <option value="IR" {{ ($selected == 'IR')?'selected':'' }}>Iran, Islamic Republic of</option>
  <option value="IQ" {{ ($selected == 'IQ')?'selected':'' }}>Iraq</option>
  <option value="IE" {{ ($selected == 'IE')?'selected':'' }}>Ireland</option>
  <option value="IM" {{ ($selected == 'IM')?'selected':'' }}>Isle of Man</option>
  <option value="IL" {{ ($selected == 'IL')?'selected':'' }}>Israel</option>
  <option value="IT" {{ ($selected == 'IT')?'selected':'' }}>Italy</option>
  <option value="JM" {{ ($selected == 'JM')?'selected':'' }}>Jamaica</option>
  <option value="JP" {{ ($selected == 'JP')?'selected':'' }}>Japan</option>
  <option value="JE" {{ ($selected == 'JE')?'selected':'' }}>Jersey</option>
  <option value="JO" {{ ($selected == 'JO')?'selected':'' }}>Jordan</option>
  <option value="KZ" {{ ($selected == 'KZ')?'selected':'' }}>Kazakhstan</option>
  <option value="KE" {{ ($selected == 'KE')?'selected':'' }}>Kenya</option>
  <option value="KI" {{ ($selected == 'KI')?'selected':'' }}>Kiribati</option>
  <option value="KP" {{ ($selected == 'KP')?'selected':'' }}>Korea, Democratic People's Republic of</option>
  <option value="KR" {{ ($selected == 'KR')?'selected':'' }}>Korea, Republic of</option>
  <option value="KW" {{ ($selected == 'KW')?'selected':'' }}>Kuwait</option>
  <option value="KG" {{ ($selected == 'KG')?'selected':'' }}>Kyrgyzstan</option>
  <option value="LA" {{ ($selected == 'LA')?'selected':'' }}>Lao People's Democratic Republic</option>
  <option value="LV" {{ ($selected == 'LV')?'selected':'' }}>Latvia</option>
  <option value="LB" {{ ($selected == 'LB')?'selected':'' }}>Lebanon</option>
  <option value="LS" {{ ($selected == 'LS')?'selected':'' }}>Lesotho</option>
  <option value="LR" {{ ($selected == 'LR')?'selected':'' }}>Liberia</option>
  <option value="LY" {{ ($selected == 'LY')?'selected':'' }}>Libya</option>
  <option value="LI" {{ ($selected == 'LI')?'selected':'' }}>Liechtenstein</option>
  <option value="LT" {{ ($selected == 'LT')?'selected':'' }}>Lithuania</option>
  <option value="LU" {{ ($selected == 'LU')?'selected':'' }}>Luxembourg</option>
  <option value="MO" {{ ($selected == 'MO')?'selected':'' }}>Macao</option>
  <option value="MK" {{ ($selected == 'MK')?'selected':'' }}>Macedonia, the former Yugoslav Republic of</option>
  <option value="MG" {{ ($selected == 'MG')?'selected':'' }}>Madagascar</option>
  <option value="MW" {{ ($selected == 'MW')?'selected':'' }}>Malawi</option>
  <option value="MY" {{ ($selected == 'MY')?'selected':'' }}>Malaysia</option>
  <option value="MV" {{ ($selected == 'MV')?'selected':'' }}>Maldives</option>
  <option value="ML" {{ ($selected == 'ML')?'selected':'' }}>Mali</option>
  <option value="MT" {{ ($selected == 'MT')?'selected':'' }}>Malta</option>
  <option value="MH" {{ ($selected == 'MH')?'selected':'' }}>Marshall Islands</option>
  <option value="MQ" {{ ($selected == 'MQ')?'selected':'' }}>Martinique</option>
  <option value="MR" {{ ($selected == 'MR')?'selected':'' }}>Mauritania</option>
  <option value="MU" {{ ($selected == 'MU')?'selected':'' }}>Mauritius</option>
  <option value="YT" {{ ($selected == 'YT')?'selected':'' }}>Mayotte</option>
  <option value="MX" {{ ($selected == 'MX')?'selected':'' }}>Mexico</option>
  <option value="FM" {{ ($selected == 'FM')?'selected':'' }}>Micronesia, Federated States of</option>
  <option value="MD" {{ ($selected == 'MD')?'selected':'' }}>Moldova, Republic of</option>
  <option value="MC" {{ ($selected == 'MC')?'selected':'' }}>Monaco</option>
  <option value="MN" {{ ($selected == 'MN')?'selected':'' }}>Mongolia</option>
  <option value="ME" {{ ($selected == 'ME')?'selected':'' }}>Montenegro</option>
  <option value="MS" {{ ($selected == 'MS')?'selected':'' }}>Montserrat</option>
  <option value="MA" {{ ($selected == 'MA')?'selected':'' }}>Morocco</option>
  <option value="MZ" {{ ($selected == 'MZ')?'selected':'' }}>Mozambique</option>
  <option value="MM" {{ ($selected == 'MM')?'selected':'' }}>Myanmar</option>
  <option value="NA" {{ ($selected == 'NA')?'selected':'' }}>Namibia</option>
  <option value="NR" {{ ($selected == 'NR')?'selected':'' }}>Nauru</option>
  <option value="NP" {{ ($selected == 'NP')?'selected':'' }}>Nepal</option>
  <option value="NL" {{ ($selected == 'NL')?'selected':'' }}>Netherlands</option>
  <option value="NC" {{ ($selected == 'NC')?'selected':'' }}>New Caledonia</option>
  <option value="NZ" {{ ($selected == 'NZ')?'selected':'' }}>New Zealand</option>
  <option value="NI" {{ ($selected == 'NI')?'selected':'' }}>Nicaragua</option>
  <option value="NE" {{ ($selected == 'NE')?'selected':'' }}>Niger</option>
  <option value="NG" {{ ($selected == 'NG')?'selected':'' }}>Nigeria</option>
  <option value="NU" {{ ($selected == 'NU')?'selected':'' }}>Niue</option>
  <option value="NF" {{ ($selected == 'NF')?'selected':'' }}>Norfolk Island</option>
  <option value="MP" {{ ($selected == 'MP')?'selected':'' }}>Northern Mariana Islands</option>
  <option value="NO" {{ ($selected == 'NO')?'selected':'' }}>Norway</option>
  <option value="OM" {{ ($selected == 'OM')?'selected':'' }}>Oman</option>
  <option value="PK" {{ ($selected == 'PK')?'selected':'' }}>Pakistan</option>
  <option value="PW" {{ ($selected == 'PW')?'selected':'' }}>Palau</option>
  <option value="PS" {{ ($selected == 'PS')?'selected':'' }}>Palestinian Territory, Occupied</option>
  <option value="PA" {{ ($selected == 'PA')?'selected':'' }}>Panama</option>
  <option value="PG" {{ ($selected == 'PG')?'selected':'' }}>Papua New Guinea</option>
  <option value="PY" {{ ($selected == 'PY')?'selected':'' }}>Paraguay</option>
  <option value="PE" {{ ($selected == 'PE')?'selected':'' }}>Peru</option>
  <option value="PH" {{ ($selected == 'PH')?'selected':'' }}>Philippines</option>
  <option value="PN" {{ ($selected == 'PN')?'selected':'' }}>Pitcairn</option>
  <option value="PL" {{ ($selected == 'PL')?'selected':'' }}>Poland</option>
  <option value="PT" {{ ($selected == 'PT')?'selected':'' }}>Portugal</option>
  <option value="PR" {{ ($selected == 'PR')?'selected':'' }}>Puerto Rico</option>
  <option value="QA" {{ ($selected == 'QA')?'selected':'' }}>Qatar</option>
  <option value="RE" {{ ($selected == 'RE')?'selected':'' }}>Réunion</option>
  <option value="RO" {{ ($selected == 'RO')?'selected':'' }}>Romania</option>
  <option value="RU" {{ ($selected == 'RU')?'selected':'' }}>Russian Federation</option>
  <option value="RW" {{ ($selected == 'RW')?'selected':'' }}>Rwanda</option>
  <option value="BL" {{ ($selected == 'BL')?'selected':'' }}>Saint Barthélemy</option>
  <option value="SH" {{ ($selected == 'SH')?'selected':'' }}>Saint Helena, Ascension and Tristan da Cunha</option>
  <option value="KN" {{ ($selected == 'KN')?'selected':'' }}>Saint Kitts and Nevis</option>
  <option value="LC" {{ ($selected == 'LC')?'selected':'' }}>Saint Lucia</option>
  <option value="MF" {{ ($selected == 'MF')?'selected':'' }}>Saint Martin (French part)</option>
  <option value="PM" {{ ($selected == 'PM')?'selected':'' }}>Saint Pierre and Miquelon</option>
  <option value="VC" {{ ($selected == 'VC')?'selected':'' }}>Saint Vincent and the Grenadines</option>
  <option value="WS" {{ ($selected == 'WS')?'selected':'' }}>Samoa</option>
  <option value="SM" {{ ($selected == 'SM')?'selected':'' }}>San Marino</option>
  <option value="ST" {{ ($selected == 'ST')?'selected':'' }}>Sao Tome and Principe</option>
  <option value="SA" {{ ($selected == 'SA')?'selected':'' }}>Saudi Arabia</option>
  <option value="SN" {{ ($selected == 'SN')?'selected':'' }}>Senegal</option>
  <option value="RS" {{ ($selected == 'RS')?'selected':'' }}>Serbia</option>
  <option value="SC" {{ ($selected == 'SC')?'selected':'' }}>Seychelles</option>
  <option value="SL" {{ ($selected == 'SL')?'selected':'' }}>Sierra Leone</option>
  <option value="SG" {{ ($selected == 'SG')?'selected':'' }}>Singapore</option>
  <option value="SX" {{ ($selected == 'SX')?'selected':'' }}>Sint Maarten (Dutch part)</option>
  <option value="SK" {{ ($selected == 'SK')?'selected':'' }}>Slovakia</option>
  <option value="SI" {{ ($selected == 'SI')?'selected':'' }}>Slovenia</option>
  <option value="SB" {{ ($selected == 'SB')?'selected':'' }}>Solomon Islands</option>
  <option value="SO" {{ ($selected == 'SO')?'selected':'' }}>Somalia</option>
  <option value="ZA" {{ ($selected == 'ZA')?'selected':'' }}>South Africa</option>
  <option value="GS" {{ ($selected == 'GS')?'selected':'' }}>South Georgia and the South Sandwich Islands</option>
  <option value="SS" {{ ($selected == 'SS')?'selected':'' }}>South Sudan</option>
  <option value="ES" {{ ($selected == 'ES')?'selected':'' }}>Spain</option>
  <option value="LK" {{ ($selected == 'LK')?'selected':'' }}>Sri Lanka</option>
  <option value="SD" {{ ($selected == 'SD')?'selected':'' }}>Sudan</option>
  <option value="SR" {{ ($selected == 'SR')?'selected':'' }}>Suriname</option>
  <option value="SJ" {{ ($selected == 'SJ')?'selected':'' }}>Svalbard and Jan Mayen</option>
  <option value="SZ" {{ ($selected == 'SZ')?'selected':'' }}>Swaziland</option>
  <option value="SE" {{ ($selected == 'SE')?'selected':'' }}>Sweden</option>
  <option value="CH" {{ ($selected == 'CH')?'selected':'' }}>Switzerland</option>
  <option value="SY" {{ ($selected == 'SY')?'selected':'' }}>Syrian Arab Republic</option>
  <option value="TW" {{ ($selected == 'TW')?'selected':'' }}>Taiwan, Province of China</option>
  <option value="TJ" {{ ($selected == 'TJ')?'selected':'' }}>Tajikistan</option>
  <option value="TZ" {{ ($selected == 'TZ')?'selected':'' }}>Tanzania, United Republic of</option>
  <option value="TH" {{ ($selected == 'TH')?'selected':'' }}>Thailand</option>
  <option value="TL" {{ ($selected == 'TL')?'selected':'' }}>Timor-Leste</option>
  <option value="TG" {{ ($selected == 'TG')?'selected':'' }}>Togo</option>
  <option value="TK" {{ ($selected == 'TK')?'selected':'' }}>Tokelau</option>
  <option value="TO" {{ ($selected == 'TO')?'selected':'' }}>Tonga</option>
  <option value="TT" {{ ($selected == 'TT')?'selected':'' }}>Trinidad and Tobago</option>
  <option value="TN" {{ ($selected == 'TN')?'selected':'' }}>Tunisia</option>
  <option value="TR" {{ ($selected == 'TR')?'selected':'' }}>Turkey</option>
  <option value="TM" {{ ($selected == 'TM')?'selected':'' }}>Turkmenistan</option>
  <option value="TC" {{ ($selected == 'TC')?'selected':'' }}>Turks and Caicos Islands</option>
  <option value="TV" {{ ($selected == 'TV')?'selected':'' }}>Tuvalu</option>
  <option value="UG" {{ ($selected == 'UG')?'selected':'' }}>Uganda</option>
  <option value="UA" {{ ($selected == 'UA')?'selected':'' }}>Ukraine</option>
  <option value="AE" {{ ($selected == 'AE')?'selected':'' }}>United Arab Emirates</option>
  <option value="UK" {{ ($selected == 'UK')?'selected':'' }}>United Kingdom</option>
  <option value="US" {{ ($selected == 'US')?'selected':'' }}>United States</option>
  <option value="UM" {{ ($selected == 'UM')?'selected':'' }}>United States Minor Outlying Islands</option>
  <option value="UY" {{ ($selected == 'UY')?'selected':'' }}>Uruguay</option>
  <option value="UZ" {{ ($selected == 'UZ')?'selected':'' }}>Uzbekistan</option>
  <option value="VU" {{ ($selected == 'VU')?'selected':'' }}>Vanuatu</option>
  <option value="VE" {{ ($selected == 'VE')?'selected':'' }}>Venezuela, Bolivarian Republic of</option>
  <option value="VN" {{ ($selected == 'VN')?'selected':'' }}>Viet Nam</option>
  <option value="VG" {{ ($selected == 'VG')?'selected':'' }}>Virgin Islands, British</option>
  <option value="VI" {{ ($selected == 'VI')?'selected':'' }}>Virgin Islands, U.S.</option>
  <option value="WF" {{ ($selected == 'WF')?'selected':'' }}>Wallis and Futuna</option>
  <option value="EH" {{ ($selected == 'EH')?'selected':'' }}>Western Sahara</option>
  <option value="YE" {{ ($selected == 'YE')?'selected':'' }}>Yemen</option>
  <option value="ZM" {{ ($selected == 'ZM')?'selected':'' }}>Zambia</option>
  <option value="ZW" {{ ($selected == 'ZW')?'selected':'' }}>Zimbabwe</option>
</select>