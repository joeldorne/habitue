
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" type="text/css" href="css/app.css">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

</head>




<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
  <a class="navbar-brand mb-0 h1" href="{{ url('/') }}">{{ config('app.name') }}</a>


<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

    <span class="navbar-toggler-icon"></span>

</button>
<h1>Pink</h1>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
        <li class="nav-item nav-link">
            <a href="{{ route('products.all') }}">Products</a>
        </li>
        @if (Route::has('login'))
            @auth
                <li class="nav-item nav-link">
                    <a href="{{ url('/') }}">Home</a>
                </li>
            @else
                <li class="nav-item nav-link">
                    <a href="{{ route('login') }}">Login</a>
                </li>
                <li class="nav-item nav-link">
                    <a href="{{ route('register') }}">Register</a>
                </li>
            @endauth
        @endif
    </ul>

  </div>
  </div>

</nav>




    </body>
