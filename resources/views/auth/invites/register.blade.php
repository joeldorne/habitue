@extends('layouts.guest')


@section('title')
Join {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div id="form_loader" class="ui main container segment basic m-t-50">
    <div class="ui middle aligned center aligned grid">
      <div class="eight wide column">
        <h2 class="ui teal image header">
          <div class="content">
            Signup to {{ config('app.name') }} and Join {{ $invite->group }}
          </div>
        </h2>

        <form id="form_register" class="ui large stackable form {{ ($errors->any()) ? 'error' : '' }}" method="post" action="{{ route('register.invite.store') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $invite->token }}">
          <input type="hidden" name="role" value="{{ $invite->role }}">

          <div class="ui stacked segment">
            <div class="two fields">
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required>
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui left icon input">
                <i class="envelope icon"></i>
                <input type="text" name="email" placeholder="E-mail address" value="{{ (old('email')) ? old('email'):$invite->email }}" required>
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Password" required>
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password_confirmation" placeholder="Repeat Password" required>
              </div>
            </div>

            <button type="submit" id="btn_submit" class="ui fluid large teal submit button">Join {{ $invite->group }}</button>
          </div>

          @if($errors->any())
            <div class="ui error message">
              <div class="header">There were some errors with your registration</div>
              <ul class="list">
                @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
        </form>

        <div class="ui message">
          You want to start your own group instead? <a href="/register">Sign Up</a>
        </div>
      </div>
    </div>
  </div>
@endsection


@section('script-footer')
  <script>
    $(document).ready(function () {
      $('#form_register').submit(function(e){
        $('#form_loader').addClass('loading');
      });
    });
  </script>
@endsection







  