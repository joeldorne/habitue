@extends('layouts.access')


@section('title')
Signup
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main container segment basic m-t-50">
    <div class="ui center aligned stackable grid">
      <div class="four wide column"></div>
      
      <div class="eight wide column">
        @include('auth.includes.register-title', ['role'=>$role])

        <form id="form_register" class="ui large stackable form {{ ($errors->any()) ? 'error' : '' }}" method="post" action="{{ route('register') }}">
          {{ csrf_field() }}
          <input type="hidden" name="role" value="{{ $role }}">

          <div class="ui segment basic">
            <div class="two fields">
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required>
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui left icon input">
                <i class="envelope icon"></i>
                <input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}" required>
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Password" required>
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password_confirmation" placeholder="Repeat Password" required>
              </div>
            </div>

            @if ($role == 'agency')
              @php
                $color = 'teal'
              @endphp
            @else
              @php
                $color = 'blue'
              @endphp
            @endif
            <button type="submit" id="btn_submit" class="ui fluid large {{ $color }} submit button">Signup</button>
          </div>

          @if($errors->any())
            <div class="ui error message">
              <div class="header">There were some errors with your registration</div>
              <ul class="list">
                @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
        </form>

        <div class="ui message">
          Already have an account? <a href="/login">Login</a>
        </div>
      </div>

      <div class="four wide column"></div>
    </div>
  </div>
@endsection


@section('script-footer')
  <script>
    $(document).ready(function () {
      $('#form_register').submit(function(e){
        $(this).addClass('loading');
      });
    });
  </script>
@endsection