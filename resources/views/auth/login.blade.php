@extends('layouts.access')


@section('title')
Signin
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic m-t-50">
        <div class="ui stackable grid">
            <div class="four wide column"></div>

            <div class="eight wide column">
                <div class="ui center aligned grid">
                    <h2 class="ui teal image header">
                        <div class="content">
                            Signin to {{ config('app.name') }}
                        </div>
                    </h2>
                </div>
                <br />

                <form id="form_login" class="ui large form {{ ($errors->any()) ? 'error' : '' }}" method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <label>Email</label>
                        <div class="ui left icon input">
                            <i class="envelope icon"></i>
                            <input type="text" name="email" placeholder="E-mail address" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <div class="field">
                        <label>Password</label>
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password" required>
                        </div>
                    </div>

                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" tabindex="0" class="hidden" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label>Remember Me</label>
                        </div>
                    </div>

                    <button type="submit" id="btn_submit" class="ui fluid large teal submit button">Signin</button>

                    @if($errors->any())
                        <div class="ui error message">
                            <div class="header">There were some errors with your login</div>
                            <ul class="list">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </form>

                <div class="ui message">
                    Forgot Your Password? <a href="/password/reset">Retrieve</a>
                </div>
            </div>

            <div class="four wide column"></div>
        </div>
    </div>
@endsection


@section('script-footer')
    <script>
        $(document).ready(function () {
            $('#form_login').submit(function(e){
                $(this).addClass('loading');
            });
        });

        $('.checkbox')
            .checkbox()
        ;
    </script>
@endsection