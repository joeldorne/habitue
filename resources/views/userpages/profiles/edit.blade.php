@extends('layouts.auth_all')


@section('title')
Update Profile
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <div class="ui small form {{ ($errors->any()) ? 'error' : '' }} {{ (session('success')) ? 'success' : '' }}">

            <form method="post" action="{{ route('user.profile.update') }}">
                {{ csrf_field() }}
                
                <div class="ui segment basic">
                    <div class="two fields">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i> 
                                <input type="text" name="first_name" placeholder="First Name" value="{{(old('first_name'))?old('first_name'):Auth::user()->first_name}}" required="required">
                            </div>
                        </div> 
                        
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i> 
                                <input type="text" name="last_name" placeholder="Last Name" value="{{(old('last_name'))?old('last_name'):Auth::user()->last_name}}" required="required">
                            </div>
                        </div>
                    </div>
                    
                    <button type="submit" id="btn_submit" class="ui fluid large teal submit button">Update</button>
                </div>
            </form>

            @if($errors->any() || true)
                <div class="ui error message">
                    <div class="header">There were some errors with your update.</div>
                    <ul class="list">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
@endsection


@section('script-footer')
@endsection