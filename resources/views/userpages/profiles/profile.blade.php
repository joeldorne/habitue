@extends('layouts.auth_all')


@section('title')
My Profile
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <p>First Name: {{ Auth::user()->first_name }}</p>
        <p>Last Name: {{ Auth::user()->last_name }}</p>
        <p>Email: {{ Auth::user()->email }}</p>

        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <p><a href="{{ route('user.profile.edit') }}">Update Profile</a></p>
        <p><a href="{{ route('user.profile.password.edit') }}">Change Password</a></p>
    </div>
@endsection


@section('script-footer')
@endsection